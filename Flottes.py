import random
from helper import Helper
from Entites import *
from Joueurs import *
from Structures import *
from Flottes import *
from Technologies import *

class Vaisseau(object):
    def __init__(self,parent,id,x,y,prop):
        self.parent=parent
        self.etoile = parent.parent
        self.id=id
        self.type=random.choice(["navette","croiseur","cargo"])
        self.x=x
        self.y=y
        self.vitesse=5
        self.angle=0
        self.cible=[]
        self.etoile=parent.parent
        self.etoileAffecter = None
        self.planeteAffecter = None
        self.travailleurs = []        
        self.ressources = {"Bois":0,                           
                           "Bouffe":0,                           
                           "Minerai":0,                           
                           "Gaz":0}        
        self.maxRessources = 500
        self.perspective = "Systeme"
        self.range = 40
        self.hp = 5
        self.power = 1
        self.prop=prop
        
    def changeCible(self,x,y):
        self.cible=[x,y]
        self.angle=Helper.calcAngle(self.x,self.y,self.cible[0],self.cible[1])
        #print("ANGLE",self.angle,math.degrees(self.angle), self.x,self.y,x,y)
        
    def DeplacerVaisseau(self,departX,departY,arriveeX,arriveeY):       
        angle = Helper.calcAngle(departX,departY,arriveeX,arriveeY)         # deplacement du travailleur
        x,y=Helper.getAngledPoint(angle,self.vitesse,departX,departY)
        d=Helper.calcDistance(self.x,self.y,arriveeX,arriveeY)
        self.x=x
        self.y=y
        
        
        if d < self.vitesse:
            print("deplacer")
            self.cible=[]
            if self.etoileAffecter :
                    self.parent.parent.parent.parent.warpIn(self)
            
            if self.planeteAffecter : 
                for planete in self.etoile.planetes:
                    if int(self.planeteAffecter) == int(planete.id):
                        print(planete.prop,self.prop)
                        if str(planete.prop) == str(self.prop):
                            self.parent.parent.parent.parent.atterirVaisseau(self)
                            break
                        else:
                            self.parent.parent.parent.parent.conquetePlanete(self)
                            

                                   
    def atterirVaisseau(self):
        posValide = False
        x = 25
        y = 25
        while posValide==False:
            if self.planete.caseList[str(int(x))+","+str(int(y))].content==None and self.planete.caseList[str(int(x))+","+str(int(y))].ressources==None and self.planete.caseList[str(int(x))+","+str(int(y))].batiment==None: 
                posValide = True
                self.planete.prop = self.prop
                self.planete.caseList[str(int(x))+","+str(int(y))].content = "vaisseau"
                vaisseau = VaisseauSurface(self.planete)
                vaisseau.x = x*100/2
                vaisseau.y = y*100/2
                vaisseau.id = self.planete.idBatiment
                vaisseau.case = str(int(x))+","+str(int(y))
                self.planete.listeBatiment.append(vaisseau)
                self.planete.caseList[str(int(x))+","+str(int(y))].batiment = vaisseau
                for travailleur in self.travailleurs:
                    self.planete.CC.creerTravailleurColonisation()
                self.passerRessources()
                self.parent.parent.parent.civs[self.prop].flottes.remove(self)
            else:
                x=x+1
                y=y-1
                
    def coloniserPlanete(self):
        self.planete.caseList[str(25)+","+str(25)].content = "Command Center"
        self.planete.CC = CommandCenter(self.planete)
        self.planete.CC.id=0
        self.planete.CC.x=2500/2
        self.planete.CC.y=2500/2
        self.planete.listeBatiment.append(self.planete.CC)
        self.planete.caseList[str(25)+","+str(25)].batiment = self.planete.CC
        for travailleur in self.travailleurs:
            self.planete.CC.creerTravailleurColonisation()
        self.passerRessources()
        self.parent.parent.parent.civs[self.prop].flottes.remove(self)

    def passerRessources(self):
        for ressource in self.ressources.keys():
            if ressource == "Bois":
                nom = "wood"
            elif ressource == "Bouffe":
                nom = "food"
            elif ressource == "Minerai":
                nom = "mineral"
            elif ressource == "Gaz":
                nom = "gas"
            print("passage ressources",self.planete.listeRessource[nom],self.ressources[ressource])
            self.planete.listeRessource[nom] += self.ressources[ressource]
            print(self.planete.listeRessource[nom])
            
    def prochaineAction(self):
        #print("DO SOMETING")
        self.bougerVaisseau()      

class VaisseauCargo(Vaisseau):
    def __init__(self,x,y,z,techLevel):
        pass
#        self.capacity = 0
#        self.tradeRoute = None
#        self.food = 0
#        self.wood = 0
#        self.metals = 0
#        self.fuel = 0
#        self.pop = 0
#        self.speed = 1
#        self.upkeep = 5
#        self.hp = 10
		
class VaisseauCombat(Vaisseau):
    def __init__(self,x,y,z,techLevel):
        pass
#        self.shieldTech = 0
#    	self.speed = 2
#        self.upkeep = 10
#        self.hp = 20
#    	self.attack = 3
#        self.attackSpeed = 2
		
class VaisseauExploration(Vaisseau):
    def __init__(self,x,y,z,techLevel):
        pass
#        self.scan = 0
#    	self.speed = 3
#        self.upkeep = 2
#        self.hp = 5
#    	self.attack = 1
#        self.attackSpeed = 1
		
