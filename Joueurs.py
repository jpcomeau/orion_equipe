import random
from helper import Helper

from Modele import *
from Structures import *
from Flottes import *
import Entites



#class Joueur():
#    def __init__(self, parent, pseudo, planeteTerre, solarSystems):
#        pass
#    
#    def donnerPlanete(self):
#        return self.planeteTerre
#    
#    def construire(self, params):
#        if params [0] == "Production":
#            self.parent.planets[int(params[1])].batiments.append(BatimentProduction(self.parent.planets[int(params[1])]))
#        
#        elif params [0] == "Ressource":
#            self.parent.planets[int(params[1])].batiments.append(BatimentRessource(self.parent.planets[int(params[1])]))
#    
#        elif params [0] == "Defense":
#            self.parent.planets[int(params[1])].batiments.append(BatimentDefense(self.parent.planets[int(params[1])]))
#
#        
#        elif params [0] == "Laboratoire":
#            self.parent.planets[int(params[1])].batiments.append(BatimentLaboratoire(self.parent.planets[int(params[1])]))
#
#
#    def miseAjourJoueur(self):
#        pass
#        for i in self.solarSystems=[]:
#            if i is updateSolarSystems(self,ID):
#                i.update()
#            
#        for i in self.planets:
#            a=0
#            for j in i.batiments[a].upgradeBatiments() #upgradeBatiments doit exister dans batiments
#            a = a+1
#        
#    def bougervide(self, params):
#        paramsXY=[params[0], params[1]]
#        id=(int(params[2]))%1000
#        self.vaisseaux[id].enDeplacementSurCible=False
#        self.vaisseaux[id].bougervide(paramsXY)
#
#    def bougercible(monJoueur, params):
#        id,x,y de la cible,joueur cible, joueur qui bouge, id du vaisseau du joueur
#        cibleJoueur=monJoueur.parent.joueurs[params[3]] #objet joueur qui est la cible
#        idCible=int(params[0])%1000
#        cibleVaisseau=cibleJoueur.vaisseaux[idCible] #objet
#        idMonV=int(params[5])%1000
#        monJoueur.vaisseaux[idMonV].enDeplacementSurCible=True
#        monJoueur.vaisseaux[idMonV].enDeplacement=1
#        monJoueur.vaisseaux[idMonV].cibleVaisseau=cibleVaisseau
#        monJoueur.vaisseaux[idMonV].bougervide([cibleVaisseau.x,cibleVaisseau.y])
#
#    def bougerplanete(self,listXY):
#        self.vaisseaux[0].bougervide(listXY)
#    
#    def deplacerJoueur(self):
#        pass
#    
#    def donneVaisseau(self):
#        return self.vaisseaux
#
#    def donneplaneteTerre(self):
#        return self.planeteTerre

class Civ(object):
    def __init__(self,parent,nom,e,p,couleur):
        self.parent=parent
        self.nom=nom
        self.etoileMere=e
        self.planeteMere=p
        self.couleur=couleur
        self.planetesColonisees=[p]
        self.etoilesVisites=[e]
        self.artefactsDecouverts=[]
        self.flottes=[]
        self.actions={"changeCible":self.changeCible,
                      "message":self.parent.parent.vue.ecrireMessage,
                      "construireBatimentRessource":Travailleur.ConstruireBatiment,
                      "construireBatimentProduction":Travailleur.ConstruireBatiment,
                      "construireTourelle":Travailleur.ConstruireBatiment,
                      "construireLaboratoire":Travailleur.ConstruireBatiment,
                      "construireTravailleur":CommandCenter.creerTravailleur,
                      "assignerTravailleur":Travailleur.assignerTravailleur,
                      "retirerTravailleur":Batiment.RetirerTravailleur,
                      "creerVaisseau":Entites.Planete.creerVaisseau,
                      "donnerCibleVaisseau":self.parent.donnerCibleVaisseaux,
                      "warpOut":self.parent.warpOut,
                      "warpIn":self.parent.warpIn,
                      "ameliorerBatiment":self.parent.ameliorerBatiment,
                      "conquetePlanete":self.parent.conquetePlanete,
                      "creerVaisseauCargo":Entites.Planete.creerVaisseauCargo,
                      "traite":self.parent.traite,
                      "atterirVaisseau":self.parent.atterirVaisseau}
                        
    def changeCible(self,par):
        id,x,y=par
        for i in self.artefacts["vaisseaux"]:
            if id==i.id:
                i.changeCible(x,y)
                print("CHANGER VAISSEAU CIBLE")
                
    def prochaineAction(self):
        pass
                                
    def evalueAction(self):
        pass
				
class AI ():
	def __init__(self):
		pass