import random
from helper import Helper
from Modele import *
from Structures import *
from Flottes import *


class Case():
    def __init__(self,parent,x,y):
        self.parent=parent
        self.x=0
        self.y=0
        self.aiSent=False
        self.ressources=None
        self.nomRessource=""
        self.content=None
        self.visible=False
        self.batiment=None
    
    def setCaseRessource(self,Ressource):
        self.ressources=Ressource
        
    def setCaseContent(self,Content):
        self.content=Content
        
    def setVisible(self,boolVisible):
        self.visible=Visible
		
class Etoile(object):
    def __init__(self,parent,id,x,y,terre,proprietaire):
        self.parent=parent
        self.id=id
        self.x=x
        self.y=y
        self.taille=random.randrange(100)+10
        self.etoileterre=terre
        self.planetes=[]
        self.prop = proprietaire
        self.creePlanetes(proprietaire)
        self.stations=[]
        self.explored=0
        self.satisfactionAVG=0
        self.vaisseaux=[]
        
    def assigneRessources(self,Bool):
        if(Bool == True):
            ressources ={   "food" : 25000,
                            "wood" : 25000,
                            "mineral" : 25000,
                            "gas" : 25000
                        }
        else:
            ressources ={   "food" : random.randrange(self.parent.ressourcesSettings["minFood"],self.parent.ressourcesSettings["maxFood"]),
                            "wood" : random.randrange(self.parent.ressourcesSettings["minWood"],self.parent.ressourcesSettings["maxWood"]),
                            "mineral" : random.randrange(self.parent.ressourcesSettings["minMineral"],self.parent.ressourcesSettings["maxMineral"]),
                            "gas" : random.randrange(self.parent.ressourcesSettings["minGas"],self.parent.ressourcesSettings["maxGas"])
                        }
        #print(ressources)
        return ressources
        
    def creePlanetes(self,proprietaire):
        n=random.randrange(3)
        tmin=self.taille*3
        tmax=self.taille*20
        np=random.randrange(10)
            
        for i in range(np):
            dirx=random.randrange(3)
            if dirx==0:
                dirx=1
            diry=random.randrange(3)
            if diry==0:
                diry=1
            x=random.randrange(-300,300)*dirx
            y=random.randrange(-300,300)*diry
            t=random.randrange(10)+4
            if self.etoileterre and i == np-1:
                e=Planete(self,self.parent.Pid,0,-200,t,self.assigneRessources(True),True,proprietaire)
                self.planetes.append(e)
                e.CC = CommandCenter(e)
                e.CC.id=0
                e.CC.x=2500/2
                e.CC.y=2500/2
                e.caseList[str(25)+","+str(25)].batiment=e.CC
                e.caseList[str(25)+","+str(25)].content="Command Center"
                e.listeBatiment.append(e.CC)
                self.proprietaire = e.prop
                for i in range(5):
                    e.CC.creerTravailleurColonisation()
                e.listeRessource={"food":1000,
                                  "wood":1000,
                                  "mineral":1000,
                                  "gas":500,
                                  "science":0,
                                  "satisfaction":0}
            else:
                self.planetes.append(Planete(self,self.parent.Pid,x,y,t,self.assigneRessources(False),False,""))
            self.parent.Pid+=1
            
class Planete(object):
    def __init__(self,parent,id,x,y,t,ressources, terre,prop):
        self.parent=parent
        self.prop = prop
        self.x=x
        self.y=y
        self.id = id
        self.idBatiment=1
        self.taille=t
        self.ressources=ressources
        self.terre = terre
        self.caseList = {}
        self.upkeepTotal = 0
        self.nbrTravailleursCreer = 0
        self.nbrTravailleursMorts = 0    
        self.systemeSolaire = ""
        self.nom = ""
        self.listeTravailleur = []
        self.generateCaseList(50, 50)
        self.ressourcesPlacement(self.parent.parent.gameSettings["RPC"])
        self.CC=None
        self.listeBatiment=[]
        self.listeFerme=[]
        self.listeScierie=[]
        self.listeMine=[]
        self.listeRaffinerie=[]
        self.listeLabo=[]
        self.listeTourelle=[]
        self.listeManufacture=[]
        self.possibiliteVaisseau= True
        self.listeRessource={"food":0,
                             "wood":0,
                             "mineral":0,
                             "gas":0,
                             "science":0,
                             "satisfaction":0}
        self.possibiliteVaisseau = False
        self.gouverneur = Gouverneur(self)
        
    def ressourcesPlacement(self, RPC):
        n = self.ressources["food"]
        while n:
            x = random.randrange(1, 49)
            y = random.randrange(1, 49)
            position=str(x)+","+str(y)
            if self.caseList[position].ressources == None:
                self.caseList[position].nomRessource="food"
                self.caseList[position].x=x
                self.caseList[position].y=y
                if(n%RPC == 0):
                    self.caseList[position].setCaseRessource(1000)
                    n = n - 1000
                else:
                    self.caseList[position].setCaseRessource(n%RPC)
                    n = n - n%RPC
                
        n = self.ressources["wood"]
        while n:
            x = random.randrange(1, 49)
            y = random.randrange(1, 49)
            position=str(x)+","+str(y)
            if self.caseList[position].ressources == None:
                self.caseList[position].nomRessource="wood"
                if(n%RPC == 0):
                    self.caseList[position].setCaseRessource(RPC)
                    n = n - RPC
                else:
                    self.caseList[position].setCaseRessource(n%RPC)
                    n = n - n%RPC
                
        n = self.ressources["mineral"]
        while n:
            x = random.randrange(1, 49)
            y = random.randrange(1, 49)
            position=str(x)+","+str(y)
            if self.caseList[position].ressources == None:
                self.caseList[position].nomRessource="mineral"
                if(n%RPC == 0):
                    self.caseList[position].setCaseRessource(RPC)
                    n = n - RPC
                else:
                    self.caseList[position].setCaseRessource(n%RPC)
                    n = n - n%RPC
                
        n = self.ressources["gas"]
        while n:
            x = random.randrange(1, 49)
            y = random.randrange(1, 49)
            position=str(x)+","+str(y)
            if self.caseList[position].ressources == None:
                self.caseList[position].nomRessource="gas"
                if(n%RPC == 0):
                    self.caseList[position].setCaseRessource(RPC)
                    n = n - RPC
                else:
                    self.caseList[position].setCaseRessource(n%RPC)
                    n = n - n%RPC
                    
    def creerVaisseau(self,nom):
        print("creer vaisseau")
        def CostProcess():
            travManu = 0
            for i in self.listeManufacture:
                travManu += i.nbTravailleur
            ok = True
            listeRessourceTemp = self.listeRessource.copy()
            listeRessourceTemp["wood"] -= (100 - travManu*5)
            listeRessourceTemp["mineral"] -= (500 - travManu*5)
            listeRessourceTemp["food"] -= (100 - travManu*5)
            listeRessourceTemp["gas"] -= (200 - travManu*5)
            for ressource in listeRessourceTemp:
                if listeRessourceTemp[ressource] < 0:
                    ok = False
            if ok :
                self.listeRessource = listeRessourceTemp.copy()
                return True
            else:
                self.parent.parent.sendStatusReport("MANUFACTURE", self.parent, self,"Ressources insuffisantes")
                return False
        if CostProcess(): 
            n = self.taille*3
            x2=self.x + 50
            y2=self.y - 50
            self.parent.parent.Vid+=1
            v = Vaisseau(self,self.parent.parent.Vid,x2,y2,nom)
            self.parent.parent.civs[nom].flottes.append(v)
            
    def creerVaisseauCargo(self,batiment,nom):
        n = self.taille*3
        x2 = self.x + 20
        y2 = self.y
        self.parent.parent.Vid+=1
        vaisseau = Vaisseau(self,self.parent.parent.Vid,x2,y2,nom)

        for travailleur in batiment.travailleurs:
            self.listeTravailleur.remove(travailleur)
        
        vaisseau.etoile = self.parent
        vaisseau.ressources = batiment.ressources
        vaisseau.travailleurs = batiment.travailleurs
        self.parent.parent.civs[nom].flottes.append(vaisseau)
        
        self.listeBatiment.remove(batiment)
        self.caseList[batiment.case].batiment = None
        self.caseList[batiment.case].content = None
        
        if batiment == self.parent.parent.parent.vue.batimentselection:
            self.parent.parent.parent.vue.batimentselection = 0
            
            
    def DecollageVaisseau(self,parent,etoile,x,y,ressources,travailleurs):       
        id = self.Vid
        self.Vid+=1
        vaisseau = Vaisseau(parent,id,x,y,parent.prop)

    
    def generateCaseList(self,sizeX,sizeY):
        for i in range(sizeX):
            for j in range(sizeY):
                position=str(i)+","+str(j)
                self.caseList[position]=Case(self,i,j)
                
    def upkeepProcess(self):
        workerCount = self.lengthTravailleur()
        self.upkeepTotal = workerCount
        tempFood = self.listeRessource["food"]
        tempFood -= workerCount
        if tempFood <= 0 :
            tempFood = 0
        self.listeRessource["food"] = tempFood
    
    def travailleurInactif(self):
        inactif = 0
        for i in self.listeTravailleur:
            if i.batimentAffecter == "":
                inactif += 1
        return inactif
    
    def travailleurBatiment(self):
        ferme = 0
        scierie = 0
        mine = 0
        labo = 0
        raffinerie = 0
        manufacture = 0
        tourelle = 0
        for j in self.listeBatiment:
            if j.typeBatiment == "ressource":
                if j.typeRessource == "food":
                    ferme += j.nbTravailleur
                elif j.typeRessource == "wood":
                    scierie += j.nbTravailleur
                elif j.typeRessource == "mineral":
                    mine += j.nbTravailleur
                elif j.typeRessource == "gas":
                    raffinerie += j.nbTravailleur
            elif j.typeBatiment == "production":
                manufacture += j.nbTravailleur
            elif j.typeBatiment == "defense":
                tourelle += j.nbTravailleur
            elif j.typeBatiment == "laboratoire":
                labo += j.nbTravailleur
                
        return ferme,scierie,mine,labo,raffinerie,manufacture,tourelle
                        
            
    def lengthTravailleur(self):
        worker = 0
        for t in self.listeTravailleur:
            worker += 1
        return worker
    
    def attaquerTourelle(self,cible):
        print("dans entites.attaquertourelle")
        dmg = 0
        for t in self.listeTourelle:
            dmg += t.calculDmg()
        cible.hp -= dmg
        if cible.hp <= 0:
            self.parent.parent.civs[cible.prop].flottes.remove(cible)
    
class Gouverneur():
    def __init__(self,parent):
        self.parent = parent
        self.ressources = {"food":[0,0],"wood":[0,0],"mineral":[0,0],"gas":[0,0],"satisfaction":[0,0],"science":[0,0]}
        #EX.: "food":[priorityLevel,currentAmount,neededAmount]
        self.associatedBuilding = {"food":"ferme","wood":"scierie","mineral":"mine","gas":"raffinerie"}
        self.couts={"travailleur":[50,0,0,0],
                    "ferme":[0,200,50,0],
                    "scierie":[0,150,100,0],
                    "mine":[0,100,150,0],
                    "raffinerie":[100,100,100,0],
                    "vaisseaux":[100,100,500,200],
                    "tourelle":[150,250,250,50],
                    "manufacture":[150,200,200,0],
                    "laboratoire":[200,500,500,0]}
        # {buildingName:[food,wood,mineral,gas]
        self.eventList={"Low Food":0, "Low Wood":0, "Low Mineral":0, "Low Gas":0}
        self.listeTravailleurIdle = []
        self.listeTravailleurActifs = []
        self.tasks = []
        self.foodCases = []
        self.woodCases = []
        self.mineralCases = []
        self.gasCases = []
        self.fermes = []
        self.buildRessourcesMap()
    
    def evaluateWorkers(self):
        
        self.listeTravailleurActifs = []
        self.listeTravailleurIdle = []
        
        for travailleur in self.parent.listeTravailleur:
            if travailleur.batimentAffecter == "" and travailleur.cible == [] and travailleur.batimentConstruire== "":
                self.listeTravailleurIdle.append(travailleur)
            elif travailleur.batimentAffecter != "":
                for batiment in self.parent.listeBatiment:
                    if batiment.id == int(travailleur.batimentAffecter):
                        if batiment.typeBatiment == "ressource":
                            if batiment.typeRessource != "food":
                                self.listeTravailleurActifs.append(travailleur)
        
    def buildStructures(self, type, assign):
        
        assignTemp = assign
        amount = math.ceil(assign/3) 
        realAmount = amount 
        i = 0
        woodNeeded = 0
        minzNeeded = 0
        woodDispo = math.floor(self.parent.listeRessource["wood"]/self.couts["ferme"][1])
        minzDispo = math.floor(self.parent.listeRessource["mineral"]/self.couts["ferme"][2])

        if  woodDispo<amount:
            realAmount = woodDispo
            woodNeeded = (amount-woodDispo)*self.couts["ferme"][1]

        if  minzDispo<amount and minzDispo<woodDispo:
            realAmount = minzDispo
            minzNeeded = (amount-minzDispo)*self.couts["ferme"][2]
            
        if realAmount != amount:
            self.broadcastEvent("La planete necessite "+str(woodNeeded)+" bois et "+str(minzNeeded)+" mineraux pour combattre la famine.")
            
        while i < realAmount:
            self.evaluateWorkers()                    
            if len(self.listeTravailleurIdle)>0:
                travailleurActif = self.listeTravailleurIdle[0]
                for case in self.parent.caseList:
                    activeCase = self.parent.caseList[case]
                    if activeCase.batiment==None:
                        if activeCase.nomRessource==type and activeCase.aiSent==False:
                            if activeCase.ressources != None:
                                activeCase.aiSent=True
                                travailleurActif.batimentConstruire=self.associatedBuilding[type]
                                travailleurActif.cible = [(activeCase.x*50)+25,(activeCase.y*50)+25]
                                break
            i= i+1
                                  
            travailleurActif.cible = [(activeCase.x*50)+25,(activeCase.y*50)+25]
            self.tasks.append(["build",travailleurActif,activeCase,(activeCase.x*50)+25,(activeCase.y*50)+25,assign])
        
    def assignWorker(self,nbWorkers,activeCase):
        
        self.evaluateWorkers()
        for batiment in self.parent.listeBatiment:
            if batiment.x == ((activeCase.x*50)):
                if batiment.y == ((activeCase.y*50)):
                    i = 0
                    while i < nbWorkers and i < (batiment.LimiteTravailleurTolere - batiment.nbTravailleur):
                        if self.listeTravailleurIdle:
                            self.listeTravailleurIdle[0].batimentAffecter = batiment.id
                            self.listeTravailleurIdle[0].cible = [(activeCase.x*50)+25,(activeCase.y*50)+25]
                            self.evaluateWorkers()
                        else:
                            if self.parent.listeRessource["food"] > 100:
                                self.parent.CC.creerTravailleur()
                                self.evaluateWorkers()
                                self.listeTravailleurIdle[0].batimentAffecter = batiment.id
                                self.listeTravailleurIdle[0].cible = [(activeCase.x*50)+25,(activeCase.y*50)+25] 
                            else:
                                self.broadcastEvent("La planete n'a pas assez de travailleur pour resoudre la famine")
                        i = i+1
    
    def broadcastEvent(self,event):
        if self.parent.parent.parent.parent.nom == self.parent.prop:
            self.parent.parent.parent.sendStatusReport("GOUVERNEUR", self.parent.parent, self.parent, event)
        
    def evaluateRessources(self):
        
        self.ressources["food"][0] = 0
        for batiment in self.parent.listeBatiment:
                if batiment.typeBatiment=="ressource":
                    if batiment.typeRessource=="food":
                        self.fermes.append(batiment)
                        self.ressources["food"][0] += batiment.nbTravailleur*5
        self.ressources["food"][1]=len(self.parent.listeTravailleur)
        ratio = self.ressources["food"][0] - self.ressources["food"][1]
        
        if self.eventList["Low Food"] != 1:
            if (ratio <= 0):
                if (self.parent.listeRessource["food"]+(ratio*30))<=0:
                    self.eventList["Low Food"] = 1
                    self.broadcastEvent("FAMINE ! ZOMFG !")
                    self.buildStructures("food",math.ceil((ratio*-1)/5))    
        elif(ratio >= 0):
            self.eventList["Low Food"] = 0
            self.broadcastEvent("Situation retablie (famine)")

    def buildRessourcesMap(self):
        
        for case in self.parent.caseList:
            activeCase = self.parent.caseList[case]
            if activeCase.nomRessource!="":
                if activeCase.batiment==None:
                    if activeCase.nomRessource=="food":
                        self.foodCases.append(activeCase)
                    elif activeCase.nomRessource=="wood":
                        self.woodCases.append(activeCase)
                    elif activeCase.nomRessource=="mineral":
                        self.mineralCases.append(activeCase)
                    elif activeCase.nomRessource=="gas":
                        self.gasCases.append(activeCase)

    def process(self):
        
        self.evaluateRessources()
        if self.tasks:
            for task in self.tasks:
                if self.parent.caseList[str(task[2].x)+","+str(task[2].y)].batiment != None:
                    self.assignWorker(task[5],task[2])
                    self.tasks.remove(task)
                #elif task[1].cible:
                    #if task[1].cible[0] != task[3] or task[1].cible[1] != task[4]:
                        #self.buildStructures("food",task[5])
                        #self.tasks.remove(task)
