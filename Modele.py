#-*- coding: ISO-8859-1 -*-
import random
from helper import Helper

from Entites import *
from Joueurs import *
from Structures import *
from Flottes import *
from Technologies import *


class Modele(object):
    id=0
    def nextId():
        Modele.id=Modele.id+1
        return Modele.id
    
    def __init__(self,parent):
        self.parent=parent
        self.gameSettings={"x_espace":1300,
                           "y_espace":800,
                           "z_espace":600,
                           "etoiles":10,
                           "pseudoetoiles":600,
                           "maxSolarSystems":10,
                           "maxPlanets":100,
                           "artefactCount":10,
                           "relicCount":10,
                           "planetsPerSystem":10,
                           "RPC":4000
                           }
        self.ressourcesSettings={"maxMineral":20000,
                                 "maxFood":20000,
                                 "maxGas":20000,
                                 "maxWood":20000,
                                 "minMineral":5000,
                                 "minFood":5000,
                                 "minGas":5000,
                                 "minWood":5000
                                }
        self.couts={"travailleur":"50 food",
                    "ferme":"200 wood, 50 mineral",
                    "scierie":"150 food, 100 mineral",
                    "mine":"100 wood, 150 mineral",
                    "raffinerie":"100 food, 100 wood, 100 mineral",
                    "vaisseaux":"100 food, 100 wood, 500 mineral, 200 gas",
                    "tourelle":"150 food, 250 wood, 250 mineral, 50 gas",
                    "manufacture":"150 food, 200 wood, 200 mineral",
                    "laboratoire":"200 food, 500 wood, 500 mineral"}
        self.rdseed=0
        self.civs={}
        self.vaisseau=""
        self.vaisseaux=[]
        self.actions=[]
        self.actionsAFaire={}
        self.etoiles=[]
        self.pseudoetoiles=[]
        self.Pid=0
        self.Eid=0
        self.Vid=0
      
    def verifTourelle(self):
        for e in self.etoiles:
            for p in e.planetes:
                if len(p.listeTourelle) != 0:
                    attaquer = False
                    print("Tourelle")
                    for keys in self.civs.keys() :
                        print("Attaque!")
                        for v in self.civs[keys].flottes :
                            if v.perspective == "Systeme":
                                if attaquer == False:
                                    if p.prop != v.prop:
                                        if int(v.etoile.id) == int(e.id):
                                            d=Helper.calcDistance(p.x,p.y,v.x,v.y)
                                            if d <= 60 :
                                                p.attaquerTourelle(v)
                                                attaquer = True
										
    def initPartie(self,listeNomsJoueurs):
        for i in range(self.gameSettings["etoiles"]):
            x=random.randrange(self.gameSettings["x_espace"])
            y=random.randrange(self.gameSettings["y_espace"])
            self.etoiles.append(Etoile(self,self.Eid,x,y,False,""))
            self.Eid+=1
        
        for i in range(self.gameSettings["pseudoetoiles"]):
            x=random.randrange(self.gameSettings["x_espace"])
            y=random.randrange(self.gameSettings["y_espace"])
            self.pseudoetoiles.append([x,y])
        couleurs=["red","blue","green","yellow","orange","purple","pink","lightblue"]
        n=0
        for j in listeNomsJoueurs:
            x=random.randrange(self.gameSettings["x_espace"]/2)
            y=random.randrange(self.gameSettings["y_espace"]/2)
            s=1
            while s:
                e=Etoile(self,self.Eid,x,y,True,j)
                self.etoiles.append(e)
                self.Eid+=1
                if len(e.planetes)>4:
                    s=0      
                    
                for p in e.planetes:
                    if p.listeRessource["food"] > 0 and p.prop == j:
                        self.civs[j]=Civ(self,j,e,p,couleurs[n])   
            n=n+1
            
    def calculRessource(self):
        for e in self.etoiles:
            for p in e.planetes:
                for b in p.listeBatiment:
                    if b.typeBatiment == "ressource":
                        b.calculRessource()
                    elif b.typeBatiment == "science":
                        b.calculRessource()
                        
    def calculUpkeep(self):
        for e in self.etoiles:
            for p in e.planetes:
                p.upkeepProcess()
        
    def prochaineAction(self,cadre):
        if cadre in self.actionsAFaire:
            for i in self.actionsAFaire[cadre]:
                if i[1] == "changeCible":
                    self.civs[i[0]].actions[i[1]](i[2])
                elif i[1] == "message":
                    self.civs[i[0]].actions[i[1]](i[2],i[0])
                elif i[1] == "construireBatimentRessource":
                    liste = i[2]
                    for e in self.etoiles:
                        for p in e.planetes:
                            if p.id == liste[0]:
                                planete = p
                    self.civs[i[0]].actions[i[1]](planete.listeTravailleur[0],planete,BatimentRessource(planete,liste[1]),liste[2],liste[3],liste[4])
                elif i[1] == "construireBatimentProduction":
                    liste = i[2]
                    for e in self.etoiles:
                        for p in e.planetes:
                            if p.id == liste[0]:
                                planete = p
                    self.civs[i[0]].actions[i[1]](planete.listeTravailleur[0],planete,BatimentProduction(planete),liste[1],liste[2],liste[3])
                elif i[1] == "construireTourelle":
                    liste = i[2]
                    for e in self.etoiles:
                        for p in e.planetes:
                            if p.id == liste[0]:
                                planete = p
                    self.civs[i[0]].actions[i[1]](planete.listeTravailleur[0],planete,Tourelle(planete),liste[1],liste[2],liste[3])
                elif i[1] == "construireLaboratoire":
                    liste = i[2]
                    for e in self.etoiles:
                        for p in e.planetes:
                            if p.id == liste[0]:
                                planete = p
                    self.civs[i[0]].actions[i[1]](planete.listeTravailleur[0],planete,Laboratoire(planete),liste[1],liste[2],liste[3])
                elif i[1] == "construireTravailleur":
                    liste = i[2]
                    for e in self.etoiles:
                        for p in e.planetes:
                            if p.id == liste[0]:
                                planete = p
                    self.civs[i[0]].actions[i[1]](planete.CC)
                elif i[1] == "assignerTravailleur":
                    liste = i[2]
                    for e in self.etoiles:
                        for p in e.planetes:
                            if p.id == liste[0]:
                                t = p.listeTravailleur[liste[1]]
                    self.civs[i[0]].actions[i[1]](t,liste[2])
                elif i[1] == "retirerTravailleur":
                    liste = i[2]
                    for e in self.etoiles:
                        for p in e.planetes:
                            if p.id == liste[0]:
                                batiment = p.listeBatiment[liste[1]]
                    self.civs[i[0]].actions[i[1]](batiment)
                elif i[1] == "creerVaisseau":
                    liste = i[2]
                    for e in self.etoiles:
                        for p in e.planetes:
                            if p.id == liste[0]:
                                planete = p
                    print("Creer Vaisseau",liste,i[0])
                    self.civs[i[0]].actions[i[1]](planete,i[0])
                elif i[1] == "donnerCibleVaisseau":
                    liste = i[2]
                    self.civs[i[0]].actions[i[1]](liste[0],liste[1],liste[2],liste[3],liste[4],i[0])
                elif i[1] == "warpOut":
                    print("warpOut")
                    liste = i[2]
                    self.civs[i[0]].actions[i[1]](liste[0],i[0])
                elif i[1] == "warpIn":
                    print("warpIn")
                    liste = i[2]
                    self.civs[i[0]].actions[i[1]](liste[0],liste[1],i[0])
                elif i[1] == "atterirVaisseau":
                    print("atterirVaisseau")
                    liste = i[2]
                    self.civs[i[0]].actions[i[1]](liste[0],liste[1],i[0]) 
                elif i[1] == "conquetePlanete":
                    print("conquetePlanete")
                    liste = i[2]
                    self.civs[i[0]].actions[i[1]](liste[0],liste[1],i[0])
                elif i[1] == "decollageVaisseau":
                    print("decollageVaisseau")
                    liste = i[2]
                    self.civs[i[0]].actions[i[1]](liste[0],liste[1])
                elif i[1] == "creerVaisseauCargo":
                    liste = i[2]
                    for e in self.etoiles:
                        for p in e.planetes:
                            if p.id == liste[0]:
                                planete = p
                                for b in planete.listeBatiment:
                                    if int(b.id) == int(liste[1]):
                                        batiment = b
                    self.civs[i[0]].actions[i[1]](planete,batiment,i[0])
                    print("Creer Vaisseau Cargo",liste,i[0])
                elif i[1] == "traite":
                    liste = i[2]
                    self.civs[i[0]].actions[i[1]](liste[0],i[0])
                    print("traite",liste,i[0])
        
            del self.actionsAFaire[cadre]
                
        for i in self.civs.keys():
            self.civs[i].prochaineAction()
            
        for i in self.civs.keys():
            self.civs[i].evalueAction()
            
    def traite(self,listeTraite,nomTransmis):
        i = 0
        for composants in listeTraite:
                if composants[1] >= 0:
                    self.civs[composants[0]].planeteMere.listeRessource["food"] += composants[1]
                    self.civs[nomTransmis].planeteMere.listeRessource["food"] -= composants[1]
                if composants[2] >= 0:
                    self.civs[composants[0]].planeteMere.listeRessource["wood"] += composants[2]
                    self.civs[nomTransmis].planeteMere.listeRessource["wood"] -= composants[2]
                if composants[3] >= 0:
                    self.civs[composants[0]].planeteMere.listeRessource["mineral"] += composants[3]
                    self.civs[nomTransmis].planeteMere.listeRessource["mineral"] -= composants[3]
                if composants[4] >= 0:
                    self.civs[composants[0]].planeteMere.listeRessource["gas"] += composants[4]
                    self.civs[nomTransmis].planeteMere.listeRessource["gas"] -= composants[4]
                    
    def decollageVaisseau(self,pId,bId):
        for e in self.etoiles:
            for p in e.planetes:
                if int(p.id) == int(pId):
                    planete = p
                    for b in planete.listeBatiment:
                        if int(b.id) == int(bId):
                            batiment = b
                            break             
        
    def ameliorerBatiment(self,type,pId):
        for e in self.etoiles:
            for p in e.planetes:
                if pId == p.id:
                    planete = p
                    break
                    
        planete.CC.ameliorerBatiment(type,planete)
            
    def atterirVaisseau(self,idV,idP,nom):
        print("Atteriir")
        for v in self.civs[nom].flottes:
            if int(idV) == int(v.id):
                vaisseau = v
        
        for e in self.etoiles:
            for p in e.planetes:
                if int(idP) == int(p.id):
                    planete = p
                
        vaisseau.planete = planete
        vaisseau.atterirVaisseau()
        
        if vaisseau in self.parent.vue.vaisseauxSelection:
            self.parent.vue.vaisseauxSelection.remove(vaisseau)
        
    def conquetePlanete(self,idV,idP,nom):
        print("Conquete")
        for v in self.civs[nom].flottes:
            if int(idV) == int(v.id):
                vaisseau = v
        
        for e in self.etoiles:
            for p in e.planetes:
                if int(idP) == int(p.id):
                    planete = p
                
        vaisseau.planete = planete
        self.civs[nom].planetesColonisees.append(planete)
        planete.prop = nom
        vaisseau.planeteAffecter = None
        vaisseau.coloniserPlanete()
        self.parent.vue.initSysteme()
        
        if vaisseau in self.parent.vue.vaisseauxSelection:
            self.parent.vue.vaisseauxSelection.remove(vaisseau)
            
    def donnerCibleVaisseaux(self,vaisseauxselection,eventx,eventy,divx,divy,nom):
        for id in vaisseauxselection:
            for i in self.civs[nom].flottes:
                if i.etoile == None:
                    x = eventx
                    y = eventy
                else:
                    x = eventx - divx
                    y = eventy - divy
                    
                if id == i.id:
                    i.cible=[]
                    
                    if i.perspective == "Systeme":
                        i.cible.append(self.parent.vue.canevasSysteme.canvasx(x))
                        i.cible.append(self.parent.vue.canevasSysteme.canvasy(y))
                    else:
                        i.cible.append(self.parent.vue.canevasEspace.canvasx(x))
                        i.cible.append(self.parent.vue.canevasEspace.canvasy(y))
    
    def vaisseauAttaque(self):
        for key in self.civs.keys():
            for va in self.civs[key].flottes:
                attaquer = False
                for nom in self.civs.keys():
                    for ve in self.civs[nom].flottes:
                        if attaquer == False:
                            if ve.prop != va.prop:
                                if va.perspective == ve.perspective:
                                    if va.perspective == "Systeme":
                                        if int(va.etoile.id) == int(ve.etoile.id):
                                            if math.fabs(va.x-ve.x) <= 40 and math.fabs(va.y-ve.y) <= 40:
                                                print("Exterminate!")
                                                ve.hp -= va.power
                                                attaquer = True
                                                if ve.hp <= 0:
                                                    self.civs[nom].flottes.remove(ve)
                                                    break
                                    else:
                                        if math.fabs(va.x-ve.x) <= 40 and math.fabs(va.y-ve.y) <= 40:
                                            print("Exterminate!")
                                            ve.hp -= va.power
                                            attaquer = True
                                            if ve.hp <= 0:
                                                self.civs[nom].flottes.remove(ve)
                                                break
                                    
    def warpOut(self,listeid,nom):
         x = None
         y = None
         for id in listeid:
            for vaisseau in self.civs[nom].flottes:
                if id == vaisseau.id:
                    if x:
                        vaisseau.x = x+10
                        vaisseau.y = y+10
                        x+=5
                        y+=5
                    else:
                        x = vaisseau.etoile.x
                        y = vaisseau.etoile.y
                        vaisseau.x = x +10
                        vaisseau.y = y +10
                    vaisseau.cible = []
                    vaisseau.etoile = None
                    vaisseau.perspective = "Espace"
                    
                if vaisseau in self.parent.vue.vaisseauxSelection:
                    self.parent.vue.vaisseauxSelection =[]
                    self.parent.vue.canevasActions.grid_forget()
         
    def warpIn(self,idV,idE,nom):
        for vaisseau in self.civs[nom].flottes:
            if idV == vaisseau.id:
                for etoile in self.etoiles:
                    if int(idE) == int(etoile.id):
                        vaisseau.etoile = etoile
                        vaisseau.x = 25
                        vaisseau.y = 25
                        vaisseau.etoileAffecter = None
                        vaisseau.perspective = "Systeme"
                        vaisseau.cible =[]
                        break
                
                if vaisseau in self.parent.vue.vaisseauxSelection:
                    self.parent.vue.vaisseauxSelection =[]
                    self.parent.vue.canevasActions.grid_forget()
            
    def deplacerTravailleur(self):
        for e in self.etoiles:
            for p in e.planetes:
                for t in p.listeTravailleur:
                    if t.cible:
                        t.DeplacerTravailleur(t.x,t.y,t.cible[0],t.cible[1])
                        
    def deplacerVaisseau(self):
        for e in self.civs.keys():
            for p in self.civs[e].flottes:
                if p.cible:
                    p.DeplacerVaisseau(p.x,p.y,p.cible[0],p.cible[1])
        
    def processGov(self):
        for e in self.etoiles:
            for p in e.planetes:
                if p.terre:
                    p.gouverneur.process()
                    
    def sendStatusReport(self,source, system, planet, event):
        self.parent.vue.ecrireStatus(source, system, planet, event)
    
    def getRessourcesSystem(self,etoile):
        food = 0
        wood = 0
        mineral = 0
        gas = 0
        science = 0
        for e in self.etoiles:
            if e.id == etoile:
                for p in e.planetes:                    
                    food += p.listeRessource["food"]
                    wood += p.listeRessource["wood"]
                    mineral += p.listeRessource["mineral"]
                    gas += p.listeRessource["gas"]
                    science += p.listeRessource["science"]
        return food,wood,mineral,gas,science
    
    def getRessourcesPlanete(self,etoile,planete):
        food = 0
        wood = 0
        mineral = 0
        gas = 0
        science = 0
        for p in etoile.planetes:
            if p.id == planete:
                food += p.listeRessource["food"]
                wood += p.listeRessource["wood"]
                mineral += p.listeRessource["mineral"]
                gas += p.listeRessource["gas"]
                science += p.listeRessource["science"]
        return food,wood,mineral,gas,science
    
    def getRessourcesEspace(self, etoile):
        food = 0
        wood = 0
        mineral = 0
        gas = 0
        science = 0
        
        food2,wood2,mineral2,gas2,science2 = self.getRessourcesSystem(etoile)
        food += food2
        wood += wood2
        mineral += mineral2
        gas += gas2
        science += science2
        
        return food,wood,mineral,gas,science   
