# -*- coding: ISO-8859-1 -*-
from tkinter import *
from tkinter.font import *
import tkinter.simpledialog as sd
import tkinter.messagebox as mb
import random
import math
import tkHyperlinkManager
from datetime import datetime
from helper import *
from PIL import Image, ImageTk
from ToolTips import *


class Buttonjm(Button):
    def __init__(self,parent,**kw):
        f=Font(size=7,slant="italic",weight="bold")
        kw["font"]=f
        kw["fg"]="orange"
        kw["bg"]="grey25"
        kw["relief"]="groove"
        Button.__init__(self,parent,**kw)
        
class ButtonPC(Buttonjm):
    def __init__(self,parent,**kw):
        kw["padx"]=15
        kw["pady"]=7
        kw["width"]=10
        kw["wraplength"]=100
        Buttonjm.__init__(self,parent,**kw)
        
class Labeljm(Label):
    def __init__(self,parent,**kw):
        f=Font(size=7)
        kw["font"]=f
        kw["fg"]="orange"
        kw["bg"]="grey25"
        Label.__init__(self,parent,**kw)

class Vue(object):
    perspective=["cosmos","espace","systeme","planete"]
    
    def __init__(self,parent,x,y,x_espace,y_espace):
        self.parent=parent
        self.zoomFactor=1
        self.modele=self.parent.modele
        self.vaisseauxSelection=[]
        self.mesphotos=[]
        self.pCourante = "Systeme"
        self.etoileselection=0
        self.planeteselection=0
        self.travailleurselection=[]
        self.batimentselection=0
        self.con=0
        self.x=x
        self.y=y
        self.x_espace=x_espace
        self.y_espace=y_espace
        self.root=Tk()
        self.root.protocol('WM_DELETE_WINDOW', self.intercepteFermeture)
        self.cadreActif=0
        self.perspective=""
        self.perspectives={}
        self.currentStar = None
        self.currentPlanet = None
        self.perspectiveCourante=None
        self.canevasCourant=None
        self.selections=[]
        self.creeCadres()
        self.placeCadre(self.cadreConnection)
        self.ligne=False
        self.rectStartX=None
        self.rectStartY=None
        self.rectX = None
        self.rectY = None
        self.offsetX = 0
        self.offsetY = 0
        self.image = tkinter.PhotoImage(file="trollface_mini.gif")

    def creeCadres(self):
        self.creeCadreConnection()
        self.creeCadreAttente()
        self.creeCadrePartie()
        
    def creeCadrePartie(self):
        self.cadrePartie=Frame(self.root)
        self.cadrePartie.rowconfigure(1,weight=1)
        self.cadrePartie.columnconfigure(0,weight=1) 
        self.creeCadreMenuPartie()
        self.creeCadreCosmos()
        self.creeCadreEspace()
        self.creeCadreSysteme()
        self.creeCadrePlanete()
        self.perspectives={"Cosmos":self.cadreCosmos,
                           "Espace":self.cadreEspace,
                           "Systeme":self.cadreSysteme,
                           "Planete":self.cadrePlanete}
        self.canevas={"Cosmos":self.canevasCosmos,
                           "Espace":self.canevasEspace,
                           "Systeme":self.canevasSysteme,
                           "Planete":self.canevasPlanete}
        self.creeCadreTraiteChat()
        self.creeCadreNiveauVue()

    def creeCadreMenuPartie(self):
        self.cadreMenuPartie=Frame(self.cadrePartie,height=40,bg="grey25")
        self.cadreMenuPartie.grid_propagate(0)
        
        trouveB=Buttonjm(self.cadreMenuPartie,text="TrouvePartie planete",command=self.centrerPlanete)
        trouveB.grid(row=0,column=0,sticky=N)
        zoomin=Buttonjm(self.cadreMenuPartie,text="ZoomIn",relief=GROOVE,command=self.zoomInEspace)
        zoomin.grid(row=0,column=1,sticky=N)
        zoomout=Buttonjm(self.cadreMenuPartie,text="ZoomOut",relief=GROOVE,command=self.zoomOutEspace)
        zoomout.grid(row=0,column=2,sticky=N)
        distance=20
        
        self.labelBouffe = Labeljm(self.cadreMenuPartie,text="Nourriture : 0",padx=distance)
        self.labelBouffe.grid(row=0,column=3)
        e = Font(size=10)
        self.labelBois = Labeljm(self.cadreMenuPartie,text="Bois : 0",padx=distance)
        self.labelBois.grid(row=0,column=4)
        e = Font(size=10)
        self.labelMetal = Labeljm(self.cadreMenuPartie,text="Mineraux : 0",padx=distance)
        self.labelMetal.grid(row=0,column=5)
        e = Font(size=10)
        self.labelGaz = Labeljm(self.cadreMenuPartie,text="Gaz : 0",padx=distance)
        self.labelGaz.grid(row=0,column=6)
        e = Font(size=10)
        self.labelScience = Labeljm(self.cadreMenuPartie,text="Science : 0",padx=distance)
        self.labelScience.grid(row=0,column=7)
        e = Font(size=10)
        e = Font(size=10)
        self.labelTime = Labeljm(self.cadreMenuPartie,text="Temps : 0 ans")
        self.labelTime.grid(row=0,column=8)
        
    def zoomInEspace(self):
        if self.zoomFactor <= 16:
            self.zoomFactor=self.zoomFactor*2
            self.x_espace=self.x_espace*2
            self.y_espace=self.y_espace*2
            self.canevasEspace.config(scrollregion=(0,0,self.x_espace,self.y_espace))
            self.canevasEspace.config(xscrollcommand=self.sxespace.set)
            self.canevasEspace.config(yscrollcommand=self.syespace.set)
            self.initEspace()      
          
    def zoomOutEspace(self):
        if self.zoomFactor >= 2:
            self.zoomFactor=self.zoomFactor/2
            self.x_espace=self.x_espace/2
            self.y_espace=self.y_espace/2
            self.canevasEspace.config(scrollregion=(0,0,self.x_espace,self.y_espace))
            self.canevasEspace.config(xscrollcommand=self.sxespace.set)
            self.canevasEspace.config(yscrollcommand=self.syespace.set)
            self.initEspace()        
        
    def creeCadreCosmos(self):
        self.cadreCosmos=Frame(self.cadrePartie)
        self.cadreCosmos.rowconfigure(0,weight=1)
        self.cadreCosmos.columnconfigure(0,weight=1)

        sy=Scrollbar(self.cadreCosmos,orient=VERTICAL)
        sx=Scrollbar(self.cadreCosmos,orient=HORIZONTAL)
        self.canevasCosmos=Canvas(self.cadreCosmos,width=self.x,height=self.y,
                            xscrollcommand=sx.set,yscrollcommand=sy.set,bd=0,
                            scrollregion=(0,0,self.x,self.y),bg="grey15")
        sx.config(command=self.canevasCosmos.xview)
        sy.config(command=self.canevasCosmos.yview)
        
        self.canevasCosmos.grid(row=0,column=0,sticky=N+S+W+E)
        sy.grid(row=0,column=1,sticky=N+S)
        sx.grid(row=1,column=0,sticky=W+E)
        self.canevasCosmos.bind("<Button-1>",self.selectObject)
        self.canevasCosmos.bind("<Configure>",self.initCosmos)
        self.canevasCosmos.bind("<Double-Button-1>",self.doubleClic)
    
    def selectObject(self,evt):
        t=self.canevasCourant.gettags(CURRENT)
        self.travailleurselection=[]
        self.batimentselection=None
        if t:
            if t[0]=="etoiles":
                for i in self.modele.etoiles:
                    if i.id==int(t[1]):
                        if i == self.etoileselection:
                            self.etoileselection=0
                        else:
                            self.etoileselection=i
                        self.cadreInfoEspace()
            elif t[0]=="planetes":
                self.vaisseauxSelection=[]
                for i in self.modele.etoiles[self.etoileselection.id].planetes:
                    if i.id == int(t[1]):
                        if i == self.planeteselection:
                            self.planeteselection=0
                            self.canevasActions.grid_forget()
                            self.cadreActions("")
                            self.cadreInfoSystem()

                        else:
                            self.planeteselection=i
                            if i.prop == self.nomjoueur.get():
                                self.cadreActions(t[0])    
                                self.cadreInfoSystem()
                            else:
                                self.cadreInfoDummy()
                                self.cadreActions("")    

                        self.vaisseauxSelection=[]                                   
            elif t[0]=="batiment":
                self.canevasActif.pack_forget()
                for i in self.modele.etoiles[self.etoileselection.id].planetes:
                    if i.id== self.planeteselection.id:
                        for j in i.listeBatiment:
                            if j.id == int(t[2]):
                                if self.travailleurselection:
                                    self.travailleurselection = []
                                if j == self.batimentselection:
                                    self.batimentselection = None
                                    self.cadreInfoPlanete()
                                    self.cadreActions.pack_forget()
                                    self.cadreInfoSystemDummy()
                                else:
                                    self.batimentselection=j
                                    self.cadreInfoPlanete()
                                    self.cadreActions(t[1])
            elif t[0]=="travailleur":
                self.travailleurselection=[]
                for i in self.modele.etoiles[self.etoileselection.id].planetes:
                    if i.id== self.planeteselection.id:
                        for j in i.listeTravailleur:
                            if j.id == int(t[1]):
                                if self.batimentselection:
                                    self.batimentselection = None
                                if j == self.travailleurselection:
                                    self.travailleurselection=[]
                                    self.cadreActions.pack_forget()
                                else:
                                    self.travailleurselection.append(j)
                                    self.cadreActions(t[0])
            elif  t[0]=="vaisseaux":
                self.planeteselection=0
                self.vaisseauxSelection=[]
                for i in self.modele.civs[self.parent.nom].flottes:
                    if i.id == int(t[1]):
                        if i in self.vaisseauxSelection:
                            self.vaisseauxSelection.remove(i)
                        else:
                            self.vaisseauxSelection.append(i)
                            self.cadreActions(t[0])
                            
            else:
                self.cadreActions("")
                self.vaisseauxSelection= []
                self.batimentselection = None
                self.travailleurselection=[]
                self.cadreInfoDummy()
        self.afficheSelection()
        
    def creeCadreEspace(self):
        self.cadreEspace=Frame(self.cadrePartie)
        self.cadreEspace.rowconfigure(0,weight=1)
        self.cadreEspace.columnconfigure(0,weight=1)

        self.syespace=Scrollbar(self.cadreEspace,orient=VERTICAL)
        self.sxespace=Scrollbar(self.cadreEspace,orient=HORIZONTAL)
        self.canevasEspace=Canvas(self.cadreEspace,width=self.x,height=self.y,
                            xscrollcommand=self.sxespace.set,yscrollcommand=self.syespace.set,bd=0,
                            scrollregion=(0,0,self.x_espace,self.y_espace),bg="grey10")
        self.sxespace.config(command=self.canevasEspace.xview)
        self.syespace.config(command=self.canevasEspace.yview)
        
        self.canevasEspace.bind("<Button-1>",self.selectObject)
        self.canevasEspace.bind("<B1-Motion>",self.dessinerRectangleEspace)
        self.canevasEspace.bind("<Button-3>",self.donnerCibleVaisseaux)
        
        self.canevasEspace.grid(row=0,column=0,sticky=N+S+W+E)
        self.syespace.grid(row=0,column=1,sticky=N+S)
        self.sxespace.grid(row=1,column=0,sticky=W+E)
        
    def creeCadreSysteme(self):
        self.cadreSysteme=Frame(self.cadrePartie)
        self.cadreSysteme.rowconfigure(0,weight=1)
        self.cadreSysteme.columnconfigure(0,weight=1)

        sy=Scrollbar(self.cadreSysteme,orient=VERTICAL)
        sx=Scrollbar(self.cadreSysteme,orient=HORIZONTAL)
        self.canevasSysteme=Canvas(self.cadreSysteme,width=self.x,height=self.y,
                            xscrollcommand=sx.set,yscrollcommand=sy.set,bd=0,
                            bg="grey15")
        sx.config(command=self.canevasSysteme.xview)
        sy.config(command=self.canevasSysteme.yview)
        
        self.canevasSysteme.bind("<Button-1>",self.selectObject)
        self.canevasSysteme.bind("<B1-Motion>",self.dessinerRectangleSysteme)
        self.canevasSysteme.bind("<Button-3>",self.donnerCibleVaisseaux)
        
        self.canevasSysteme.grid(row=0,column=0,sticky=N+S+W+E)
        sy.grid(row=0,column=1,sticky=N+S)
        sx.grid(row=1,column=0,sticky=W+E)
        self.canevasSysteme.bind("<Configure>",self.initSysteme)
        
    def initSysteme(self,evt=""):
        if self.etoileselection:
            self.afficheSysteme(self.etoileselection)
    
    def initPlanete(self,evt=""):
        if self.planeteselection:
            self.affichePlanete()
    
    def parsePosition(self,str): 
        nums = [int(n) for n in str.split(',')]
        return nums[0] , nums[1]
           
    def creeCadrePlanete(self):
        self.cadrePlanete=Frame(self.cadrePartie)
        self.cadrePlanete.rowconfigure(0,weight=1)
        self.cadrePlanete.columnconfigure(0,weight=1)

        self.sy=Scrollbar(self.cadrePlanete,orient=VERTICAL)
        self.sx=Scrollbar(self.cadrePlanete,orient=HORIZONTAL)
        
        self.canevasPlanete=Canvas(self.cadrePlanete,width=self.x,height=self.y,
                            xscrollcommand=self.sx.set,yscrollcommand=self.sy.set,bd=0,
                            scrollregion=(0,0,50*50,50*50),bg="darkgreen")
        self.canevasPlanete.bind("<Button-1>",self.selectObject)
        self.canevasPlanete.bind("<B1-Motion>",self.dessinerRectangle)
        self.canevasPlanete.bind("<Button-3>",self.donnerCible)
        self.sx.config(command=self.canevasPlanete.xview)
        self.sy.config(command=self.canevasPlanete.yview)
        
        self.canevasPlanete.grid(row=0,column=0,sticky=N+S+W+E)
        self.sy.grid(row=0,column=1,sticky=N+S)
        self.sx.grid(row=1,column=0,sticky=W+E)
        
        self.canevasPlanete.xview( "moveto", 16/50 )
        self.canevasPlanete.yview( "moveto", 17/50 )
        
    def topLevelTraite(self):
        self.nbJoueurs = len(self.modele.civs)
        if self.nbJoueurs > 1:
            self.top = Toplevel(self.root,takefocus=True,width=400,height=400,bg="grey50")
            self.canevasTraite = Canvas(self.top,width=400,height=400,bg="grey50")
            self.canevasTraite.grid(column=0,row=0)
            self.labelControls = []
            self.labelNoms = []
            self.spinnerBouffe = []
            self.spinnerWood = []
            self.spinnerMineral = []
            self.spinnerGas = []
            self.labelControls.append(Label(self.canevasTraite,text="Noms",fg="white",bg="grey50"))
            self.labelControls.append(Label(self.canevasTraite,text="Bouffe",fg="white",bg="grey50"))
            self.labelControls.append(Label(self.canevasTraite,text="Wood",fg="white",bg="grey50"))
            self.labelControls.append(Label(self.canevasTraite,text="Minerai",fg="white",bg="grey50"))
            self.labelControls.append(Label(self.canevasTraite,text="Gaz",fg="white",bg="grey50"))
            k = 0
            while k<5:
                self.labelControls[k].grid(column=k,row=0)
                k += 1
            food = self.modele.civs[self.parent.nom].planeteMere.listeRessource["food"]
            wood = self.modele.civs[self.parent.nom].planeteMere.listeRessource["wood"]
            mineral = self.modele.civs[self.parent.nom].planeteMere.listeRessource["mineral"]
            gas = self.modele.civs[self.parent.nom].planeteMere.listeRessource["gas"]
            i = 0
            for nom in self.modele.civs.keys():
                if nom != self.parent.nom:
                    self.labelNoms.append(Label(self.canevasTraite,text=nom,fg="white",bg="grey50"))
                    self.labelNoms[i].grid(column=0,row=i+1)
                    self.spinnerBouffe.append(Spinbox(self.canevasTraite,from_=0, to=food, increment=100))
                    self.spinnerBouffe[i].grid(column=1,row=i+1)
                    self.spinnerWood.append(Spinbox(self.canevasTraite,from_=0, to=wood, increment=100))
                    self.spinnerWood[i].grid(column=2,row=i+1)
                    self.spinnerMineral.append(Spinbox(self.canevasTraite,from_=0, to=mineral, increment=100))
                    self.spinnerMineral[i].grid(column=3,row=i+1)
                    self.spinnerGas.append(Spinbox(self.canevasTraite,from_=0, to=gas, increment=100))
                    self.spinnerGas[i].grid(column=4,row=i+1)
                    i += 1
            buttonOk = ButtonPC(self.canevasTraite,text="Envoyer",command=self.sendTraite)
            buttonOk.grid(column=0,row=i+1)
        else:
            self.ecrireSystemReport("System","Pas de joueur avec qui �changer!")
    
    def sendTraite(self):
        i = 0
        listeTraite = []
        for nom in self.modele.civs.keys():
            if nom != self.parent.nom:
                food = int(self.spinnerBouffe[i].get())
                wood = int(self.spinnerWood[i].get())
                mineral = int(self.spinnerMineral[i].get())
                gas = int(self.spinnerGas[i].get())
                joueurTraite = []
                joueurTraite.append(nom)
                joueurTraite.append(food)
                joueurTraite.append(wood)
                joueurTraite.append(mineral)
                joueurTraite.append(gas)
                listeTraite.append(joueurTraite)
                i += 1
        self.parent.traite(listeTraite)
        self.top.destroy()
                
        
    def creeCadreTraiteChat(self):
        self.cadreTraiteChat=Frame(self.cadrePartie,width=300,bg="grey25")
        self.cadreTraiteChat.grid(column=1,row=1)
        self.cadreTraiteChat.grid_propagate(0)
        self.cadreTraite = Frame(self.cadreTraiteChat,width=300,height=100,bg="grey25")
        self.cadreTraite.grid(column=0,row=0)
        self.buttonTraite = Buttonjm(self.cadreTraite,text="Trait�",command=self.topLevelTraite)
        self.buttonTraite.grid(column=0,row=0)        
        self.cadreChatLog = Frame(self.cadreTraiteChat,bd=5,relief=GROOVE,width=300,bg="grey25")
        self.chatText = Text(self.cadreChatLog,width=29,height=37,bg="white")
        self.hyperlink = tkHyperlinkManager.HyperlinkManager(self.chatText)
        #self.chatScroll = Scrollbar(self.cadreChatLog)
        #self.chatScroll.pack(side=RIGHT, fill=Y)
        self.chatText.config(state=DISABLED)
        self.chatText.grid(column=0,row=0)
        #self.chatScroll.config(command=self.chatText.yview)
        #self.chatText.config(yscrollcommand=self.chatScroll.set)
        self.cadreChatLog.grid(column=0,row=1)
        self.inputText = Entry(self.cadreTraiteChat,width=37)
        self.inputText.bind("<Return>",self.envoyerMessage)
        self.inputText.grid(column=0,row=2)
     
    def creeCanvasListeNoms(self):
        self.canvasListeJoueur = Canvas(self.cadreTraite,bg="grey25",height=100,width=300)
        i = 0
        for nom in self.modele.civs.keys():
            i += 1
            self.canvasListeJoueur.create_text(40,i*15,text=nom,fill="white",tags=nom)
        self.canvasListeJoueur.grid(column=0,row=1)
        
    def creeCadreNiveauVue(self):
        self.cadreNiveauVue=Frame(self.cadrePartie,height=100,bg="grey25")
        self.cadreNiveauVue.grid_propagate(0)
        self.cadreVue = Frame(self.cadreNiveauVue,width=100,height=100,bg="grey25")
        self.cadreVue.grid(column=0,row=0)
        self.cadreInfosActives()
        bcosmos=Buttonjm(self.cadreVue,text="Cosmos",width=10)
        bcosmos.grid(column=0,row=0,sticky=N)
        bcosmos.bind("<Button>",self.placeCadreNiveau)
        bespace=Buttonjm(self.cadreVue,text="Espace",width=10)
        bespace.grid(column=0,row=1,sticky=N)
        bespace.bind("<Button>",self.placeCadreNiveau)
        bsysteme=Buttonjm(self.cadreVue,text="Systeme",width=10)
        bsysteme.grid(column=0,row=2,sticky=N)
        bsysteme.bind("<Button>",self.placeCadreNiveau)
        bplanete=Buttonjm(self.cadreVue,text="Planete",width=10)
        bplanete.grid(column=0,row=3,sticky=N)
        bplanete.bind("<Button>",self.placeCadreNiveau)
    
    def cadreInfosActives(self):
        self.cadreInfo = Frame(self.cadreNiveauVue,width=470,height=100,bg="grey25")
        self.cadreInfo.grid(column=1,row=0,sticky=E)
        self.cadreInfo.grid_propagate(0)
    
    def cadreInfoEspace(self):
        sommeress = 0
        self.canevasActif = Canvas(self.cadreInfo, width=470, height=100)
        self.canevasActif.grid(column=0,row=0)
        self.canevasActif.grid_propagate(0)
        
        if self.etoileselection:
            for etoile in self.modele.etoiles :
                if etoile.id == self.etoileselection.id:
                    food,wood,mineral,gas,science = self.modele.getRessourcesEspace(self.etoileselection.id)
                    self.afficheRessources(food, wood, mineral, gas, science)
                    
                    for chaqueplanete in etoile.planetes:
                        for i in chaqueplanete.listeRessource.keys(): 
                            sommeress +=  chaqueplanete.listeRessource[i]
            
                    self.labelInfoEtoiles = Label ( self.canevasActif, text ="Infos etoiles: ")
                    self.labelInfoEtoiles.grid(column=0,row=0,sticky=NW )
                    self.labelInfoID = Label(self.canevasActif, text = "id etoile: " + str(self.etoileselection.id))
                    self.labelInfoID.grid(column=0,row=1,sticky=NW )
                    self.labelInfoSatis = Label( self.canevasActif, text = "Satisfaction : " + str(etoile.satisfactionAVG ))
                    self.labelInfoSatis.grid(column=0, row=2, sticky= NW)
                    self.labelInfoProprietaire = Label( self.canevasActif, text = "Proprietaire: " +etoile.prop)
                    self.labelInfoProprietaire.grid( column = 1, row= 1 , sticky = NW,padx=(110,0))
                    self.labelInfoNombreVaisseaux = Label( self.canevasActif, text = "Nombre de vaisseaux: " + str(len(etoile.vaisseaux)))
                    self.labelInfoNombreVaisseaux.grid( column = 1, row= 2 , sticky = NW,padx=(110,0))
                    self.labelInfoNbrePlanete = Label( self.canevasActif, text = "Nombre de planetes: " + str(len(etoile.planetes)))
                    self.labelInfoNbrePlanete.grid( column = 0, row= 3 , sticky = NW)
                    self.labelInfoNbreRess = Label( self.canevasActif, text = "Nombre de ressources: " + str(sommeress))
                    self.labelInfoNbreRess.grid( column= 1, row= 3 , sticky = NW,padx=(110,0))
        
    def cadreInfoSystem(self):
        self.canevasActif = Canvas(self.cadreInfo, width=470, height=100)
        self.canevasActif.grid(column=0,row=0)
        self.canevasActif.grid_propagate(0)
        if self.planeteselection:
            food,wood,mineral,gas,science = self.modele.getRessourcesPlanete(self.etoileselection,self.planeteselection)
            self.afficheRessources(food, wood, mineral, gas, science)
            if self.planeteselection.prop == self.parent.nom:
                self.labelInfoRess = Label ( self.canevasActif, text ="Infos Plan�te: "+str(self.planeteselection.id))
                self.labelInfoRess.grid(column=0,row=0,sticky=NW )
                self.labelInfoBois = Label ( self.canevasActif, text ="Bois: " +str(self.planeteselection.listeRessource["food"]))
                self.labelInfoBois.grid(column=0,row=1, sticky=NW)
                self.labelInfoBouffe = Label ( self.canevasActif, text ="Nourriture: "+ str(self.planeteselection.listeRessource["wood"]))
                self.labelInfoBouffe.grid(column=0,row=2,sticky=NW)
                self.labelInfoMetal = Label ( self.canevasActif, text ="Metal: "+ str(self.planeteselection.listeRessource["mineral"]))
                self.labelInfoMetal.grid(column=0,row=3,sticky=NW)
                self.labelInfoGas= Label ( self.canevasActif, text ="Gas: "+str(self.planeteselection.listeRessource["gas"]))
                self.labelInfoGas.grid(column=1,row=0,sticky=NW,padx=(150,0))
                self.labelInfoScience= Label ( self.canevasActif, text ="Science: "+str(self.planeteselection.listeRessource["science"]))
                self.labelInfoScience.grid(column=1,row=1,sticky=NW,padx=(150,0))
                self.labelInfoTravailleurActif= Label ( self.canevasActif, text ="Travailleur Actif: " + str(self.planeteselection.lengthTravailleur()))
                self.labelInfoTravailleurActif.grid(column=1,row=2,sticky=NW,padx=(150,0))
        
    def cadreInfoDummy(self):
        self.canevasActif = Canvas(self.cadreInfo, width=470, height=100)
        self.canevasActif.grid(column=0,row=0)
        self.canevasActif.grid_propagate(0)
        
    def cadreInfoPlanete(self):
        self.canevasActif = Canvas(self.cadreInfo, width=470, height=100)
        self.canevasActif.grid(column=0,row=0)
        self.canevasActif.grid_propagate(0)
        if not self.batimentselection:
            self.labelActif = Label(self.canevasActif,text="Info Plan�te: "+str(self.planeteselection.id))
            self.labelActif.grid(column=0,row=0,sticky=NW)
            labelUpKeep = Label(self.canevasActif,text="UpKeep : "+str(self.planeteselection.upkeepTotal))
            labelUpKeep.grid(column=0,row=1,sticky=NW)
            labelTravailleursCreer = Label(self.canevasActif,text="Nombre de travailleurs cr��s : "+str(self.planeteselection.nbrTravailleursCreer))
            labelTravailleursCreer.grid(column=0,row=2,sticky=NW)
            labelTravailleursMorts = Label(self.canevasActif,text="Nombre de travailleurs morts : "+str(self.planeteselection.nbrTravailleursMorts))
            labelTravailleursMorts.grid(column=0,row=3,sticky=NW)
            labelTravailleurs = Label(self.canevasActif,text="Nombre de travailleurs : "+str(self.planeteselection.lengthTravailleur()))
            labelTravailleurs.grid(column=1,row=1,sticky=NW)
            labelTravailleursInactif = Label(self.canevasActif,text="Nombre de travailleurs inactifs : "+str(self.planeteselection.travailleurInactif()))
            labelTravailleursInactif.grid(column=1,row=2,sticky=NW)
            
        elif self.batimentselection.__class__.__name__ == "CommandCenter":
            ferme,scierie,mine,labo,raffinerie,manufacture,tourelle = self.planeteselection.travailleurBatiment()
            self.labelActif = Label(self.canevasActif,text="Info Command Center")
            self.labelActif.grid(column=0,row=0,sticky=NW)
            labelTravailleursInactif = Label(self.canevasActif,text="Travailleurs inactifs : "+str(self.planeteselection.travailleurInactif()))
            labelTravailleursInactif.grid(column=1,row=0,sticky=NW)
            labelFerme = Label(self.canevasActif,text="Travailleurs dans les fermes : "+str(ferme))
            labelFerme.grid(column=0,row=1,sticky=NW)
            labelScierie = Label(self.canevasActif,text="Travailleurs dans les scieries : "+str(scierie))
            labelScierie.grid(column=0,row=2,sticky=NW)
            labelMine = Label(self.canevasActif,text="Travailleurs dans les mines : "+str(mine))
            labelMine.grid(column=0,row=3,sticky=NW)
            labelRaffinerie = Label(self.canevasActif,text="Travailleurs dans les raffinerie : "+str(raffinerie))
            labelRaffinerie.grid(column=1,row=1,sticky=NW)
            labelLaboratoire = Label(self.canevasActif,text="Travailleurs dans les laboratoires : "+str(labo))
            labelLaboratoire.grid(column=1,row=2,sticky=NW)
            labelTourelle = Label(self.canevasActif,text="Travailleurs dans les tourelles : "+str(tourelle))
            labelTourelle.grid(column=1,row=3,sticky=NW)
     
    def cadreActions(self,tag):
            self.canevasActions = Canvas(self.cadreNiveauVue,width=430,height=100,bg="grey25")
            self.canevasActions.grid(column=2,row=0,sticky=E)
            self.canevasActions.grid_propagate(0)
            if tag == "travailleur":
                self.actionsTravailleurs()
            elif tag == "commandcenter":
                self.actionsCommandCenter()
            elif tag == "planetes":
                self.actionsPlanetes()
            elif tag == "vaisseaux":
                self.actionVaisseaux()
            elif tag in {"scierie","mine","raffinerie","ferme","manufacture","laboratoire","tourelle"} :
                self.actionsBatiments()
            else:
                self.actionsDummy()

    def actionsDummy(self):
        self.canevasActions = Canvas(self.cadreNiveauVue,width=430,height=100,bg="grey25")
        self.canevasActions.grid(column=2,row=0,sticky=E)
        self.canevasActions.grid_propagate(0)
    
    def actionVaisseaux(self):
        if self.pCourante == "Systeme":  
            bWarp = ButtonPC(self.canevasActions,text="Warp")
            bWarp.grid(column=0,row=0)
            bWarp.bind("<Button>", self.warpOutVaisseaux)

        elif self.pCourante == "Espace":
            pass
        else:
            bEnleverTravailleur = ButtonPC(self.canevasActions,text="Retirer un travailleur")
            bEnleverTravailleur.grid(column=0,row=0)
            bEnleverTravailleur.bind("<Button>", self.retirerTravailleur)
            
            bDecoller = ButtonPC(self.canevasActions,text="D�collage du vaisseau")
            bDecoller.grid(column=0,row=1)
            bDecoller.bind("<Button>", self.decollerVaisseau)
            
            bAjouterBouffe = ButtonPC(self.canevasActions,text="Charger 100 Bouffe")
            bAjouterBouffe.grid(column=1,row=0)
            bAjouterBouffe.bind("<Button>", self.ajouterBouffeVaisseaux)
            
            bAjouterBois = ButtonPC(self.canevasActions,text="Charger 100   Bois")
            bAjouterBois.grid(column=1,row=1)
            bAjouterBois.bind("<Button>", self.ajouterBoisVaisseaux)
            
            bAjouterMinerai = ButtonPC(self.canevasActions,text="Charger 100 Minerai")
            bAjouterMinerai.grid(column=2,row=0)
            bAjouterMinerai.bind("<Button>", self.ajouterMineraiVaisseau)
            
            bAjouterGaz = ButtonPC(self.canevasActions,text="Charger 100   Gaz")
            bAjouterGaz.grid(column=2,row=1)
            bAjouterGaz.bind("<Button>", self.ajouterGazVaisseau)
    
    def actionsBatiments(self):
        bEnleverTravailleur = ButtonPC(self.canevasActions,text="Retirer un travailleur")
        bEnleverTravailleur.grid(column=0,row=0)
        bEnleverTravailleur.bind("<Button>", self.retirerTravailleur)
    
    def actionsPlanetes(self):
        if self.planeteselection.possibiliteVaisseau:
            bAjouterVaisseaux = ButtonPC(self.canevasActions,text="Construire un vaisseau")
            bAjouterVaisseaux.grid(column=0,row=0)
            bAjouterVaisseaux.bind("<Button>", self.construireVaisseau)
            createToolTip(bAjouterVaisseaux,self.modele.couts["vaisseaux"])
            
    def actionsCommandCenter(self):
        bTravailleur = ButtonPC(self.canevasActions,text="Cr�er un travailleur")   
        bTravailleur.grid(column=0,row=0)
        bTravailleur.bind("<Button>",self.construireTravailleur)
        createToolTip(bTravailleur,self.modele.couts["travailleur"])
        
        bFerme = ButtonPC(self.canevasActions,text="Am�liorer les fermes")   
        bFerme.grid(column=0,row=1)
        bFerme.bind("<Button>",self.ameliorerBatiment("Ferme"))
        if self.planeteselection.listeFerme != []:
            coutFerme = 100*self.planeteselection.listeFerme[0].niveauTechno
            createToolTip(bFerme,"Niveau : "+str(self.planeteselection.listeFerme[0].niveauTechno)+"\n"+str(coutFerme)+" science")
        else:
            createToolTip(bFerme,"Aucune Ferme")
        
        bScierie = ButtonPC(self.canevasActions,text="Am�liorer les scieries")   
        bScierie.grid(column=1,row=0)
        bScierie.bind("<Button>",self.ameliorerBatiment("Scierie"))
        if self.planeteselection.listeScierie != []:
            coutScierie = 100*self.planeteselection.listeScierie[0].niveauTechno
            createToolTip(bScierie,"Niveau : "+str(self.planeteselection.listeScierie[0].niveauTechno)+"\n"+str(coutScierie)+" science")
        else:
            createToolTip(bScierie,"Aucune Scierie")
    
        bMine = ButtonPC(self.canevasActions,text="Am�liorer les mines")   
        bMine.grid(column=1,row=1)
        bMine.bind("<Button>",self.ameliorerBatiment("Mine"))
        if self.planeteselection.listeMine != []:
            coutMine = 100*self.planeteselection.listeMine[0].niveauTechno
            createToolTip(bMine,"Niveau : "+str(self.planeteselection.listeMine[0].niveauTechno)+"\n"+str(coutMine)+" science")
        else:
            createToolTip(bMine,"Aucune Mine")
        
        bRaffinerie = ButtonPC(self.canevasActions,text="Am�liorer les raffineries")   
        bRaffinerie.grid(column=2,row=0)
        bRaffinerie.bind("<Button>",self.ameliorerBatiment("Raffinerie"))
        if self.planeteselection.listeRaffinerie != []:
            coutRaffinerie = 100*self.planeteselection.listeRaffinerie[0].niveauTechno
            createToolTip(bRaffinerie,"Niveau : "+str(self.planeteselection.listeRaffinerie[0].niveauTechno)+"\n"+str(coutRaffinerie)+" science")
        else:
            createToolTip(bRaffinerie,"Aucune Raffinerie")
        
        bManufacture = ButtonPC(self.canevasActions,text="Am�liorer les manufactures")   
        bManufacture.grid(column=2,row=1)
        bManufacture.bind("<Button>",self.ameliorerBatiment("Manufacture"))
        if self.planeteselection.listeManufacture != []:
            coutManufacture = 100*self.planeteselection.listeManufacture[0].niveauTechno
            createToolTip(bManufacture,"Niveau : "+str(self.planeteselection.listeManufacture[0].niveauTechno)+"\n"+str(coutManufacture)+" science")
        else:
            createToolTip(bManufacture,"Aucune Manufacture")
        
        bLabo = ButtonPC(self.canevasActions,text="Am�liorer les laboratoires")   
        bLabo.grid(column=3,row=0)
        bLabo.bind("<Button>",self.ameliorerBatiment("Laboratoire"))
        if self.planeteselection.listeLabo != []:
            coutLabo = 100*self.planeteselection.listeLabo[0].niveauTechno
            createToolTip(bLabo,"Niveau : "+str(self.planeteselection.listeLabo[0].niveauTechno)+"\n"+str(coutLabo)+" science")
        else:
            createToolTip(bLabo,"Aucune Laboratoire")
        
        bTourelle = ButtonPC(self.canevasActions,text="Am�liorer les tourelles")   
        bTourelle.grid(column=3,row=1)
        bTourelle.bind("<Button>",self.ameliorerBatiment("Tourelle"))
        if self.planeteselection.listeTourelle != []:
            coutTourelle = 100*self.planeteselection.listeTourelle[0].niveauTechno
            createToolTip(bTourelle,"Niveau : "+str(self.planeteselection.listeTourelle[0].niveauTechno)+"\n"+str(coutTourelle)+" science")
        else:
            createToolTip(bTourelle,"Aucune Tourelle")
            
    def ameliorerBatiment(self,type):
        self.parent.ameliorerBatiment(type,self.planeteselection.id)
        #self.planeteselection.CC.ameliorerBatiment(type,self.planeteselection)
        
    def actionsTravailleurs(self):
        bferme=ButtonPC(self.canevasActions,text="Construire une ferme")
        bferme.grid(column=0,row=0)
        bferme.bind("<Button>",self.construireFerme)
        createToolTip(bferme,self.modele.couts["ferme"])
        bferme.configure(fg="red",bg="black")
        bscierie=ButtonPC(self.canevasActions,text="Construire une scierie")
        bscierie.grid(column=0,row=1)
        bscierie.bind("<Button>",self.construireScierie)
        createToolTip(bscierie,self.modele.couts["scierie"])
        bscierie.configure(fg="saddle brown",bg="black")
        bmine=ButtonPC(self.canevasActions,text="Construire une mine")
        bmine.bind("<Button>",self.construireMine)
        bmine.grid(column=1,row=0)
        createToolTip(bmine,self.modele.couts["mine"])
        bmine.configure(fg="grey",bg="black")
        bgas=ButtonPC(self.canevasActions,text="Construire une raffinerie")
        bgas.bind("<Button>",self.construireRaffinerie)
        bgas.grid(column=1,row=1)
        createToolTip(bgas,self.modele.couts["raffinerie"])
        bgas.configure(fg="aquamarine",bg="black")
        bmanufacture=ButtonPC(self.canevasActions,text="Construire une manufacture")
        bmanufacture.bind("<Button>",self.construireManufacture)
        bmanufacture.grid(column=2,row=0)
        createToolTip(bmanufacture,self.modele.couts["manufacture"])
        bmanufacture.configure(fg="orange",bg="black")
 
        btourelle=ButtonPC(self.canevasActions,text="Construire une tourelle")
        btourelle.bind("<Button>",self.construireTourelle)
        btourelle.grid(column=2,row=1)
        createToolTip(btourelle,self.modele.couts["tourelle"])
        btourelle.configure(fg="purple",bg="black")

        
        blaboratoire=ButtonPC(self.canevasActions,text="Construire un laboratoire")
        blaboratoire.bind("<Button>",self.construireLaboratoire)
        blaboratoire.grid(column=3,row=0)
        createToolTip(blaboratoire,self.modele.couts["laboratoire"])
        blaboratoire.configure(fg="white",bg="black")

        
        bexploration=ButtonPC(self.canevasActions,text="Exploration de la plan�te")
        bexploration.bind("<Button>",self.explorer)
        bexploration.grid(column=3,row=1)
        
    
    def explorer(self,event):
        pass
        
    def placeCadreNiveau(self,evt):   
        t=evt.widget.cget("text")
        self.vaisseauxSelection = []
        print("PRESPECTIVE",t)
        self.placePerspective(t)
        
    def intercepteFermeture(self):
        print("Je me ferme")
        self.parent.jeQuitte()
        self.root.destroy()
        
    def afficheAttente(self):
        self.placeCadre(self.cadreAttente)
        
    def placeCadre(self,c):
        if self.cadreActif:
            self.cadreActif.pack_forget()
        if c==self.cadrePartie:
            self.creeCanvasListeNoms()
        self.cadreActif=c
        self.cadreActif.pack(expand=1,fill=BOTH)
 
    def creeCadreConnection(self):
        self.cadreConnection=Frame(self.root)
        cadreMenu=Frame(self.cadreConnection)

        Nom=Labeljm(cadreMenu,text="Votre NOM svp->")
        self.nomjoueur=Entry(cadreMenu)
        self.nomjoueur.insert(0,"jmd_"+str(random.randrange(100)))
        Nom.grid(column=0,row=0)
        self.nomjoueur.grid(column=1,row=0)
        
        
        lcree=Labeljm(cadreMenu,text="Pour cr�er un serveur � l'adresse inscrite")
        lconnect=Labeljm(cadreMenu,text="Pour vous connecter � un serveur")
        lcree.grid(column=0,row=1)
        lconnect.grid(column=1,row=1)
        
        lip=Labeljm(cadreMenu,text=self.parent.monip)
        self.autreip=Entry(cadreMenu)
        self.autreip.insert(0,self.parent.monip)
        lip.grid(column=0,row=2)
        self.autreip.grid(column=1,row=2)
        
        creerB=Buttonjm(cadreMenu,text="Creer un serveur",command=self.creerServeur)
        connecterB=Buttonjm(cadreMenu,text="Connecter a un serveur",command=self.connecterServeur)
        creerB.grid(column=0,row=3)
        connecterB.grid(column=1,row=3)

        self.galax=PhotoImage(file="galaxie.gif")
        galaxl=Labeljm(self.cadreConnection,image=self.galax)
        galaxl.pack()
        cadreMenu.pack()
        
    def creeCadreAttente(self):
        self.cadreAttente=Frame(self.root)
        cadreMenu=Frame(self.cadreAttente)
        self.listeJoueurs=Listbox(cadreMenu)
        self.demarreB=Buttonjm(cadreMenu,text="Demarre partie",state=DISABLED,command=self.parent.demarrePartie)
        self.demarreB.grid(column=0,row=1)
        self.listeJoueurs.grid(column=0,row=0)
        cadreMenu.pack(side=LEFT)
        self.galax2=PhotoImage(file="galaxie.gif")
        galax=Labeljm(self.cadreAttente,image=self.galax2)
        galax.pack(side=RIGHT)
        
    def afficheListeJoueurs(self,liste):
        self.listeJoueurs.delete(0,END)
        for i in liste:
            self.listeJoueurs.insert(END,i)
        
    def exploreSysteme(self):
        pass
    def colonisePlanete(self):
        pass
        
    def initPartie(self,modele):
        self.partie=modele
        self.moi=modele.parent.nom
        self.initCosmos()
        self.initEspace()
        
        self.cadreMenuPartie.grid(column=0,row=0,columnspan=2,sticky=W+E)
        self.cadreTraiteChat.grid(column=1,row=1,rowspan=2,sticky=W+E+S+N)
        self.cadreNiveauVue.grid(column=0,row=2,sticky=W+E+S+N)
        self.etoileselection = self.partie.civs[self.moi].etoileMere
        self.perspectiveCourante=self.perspectives["Systeme"]
        self.placePerspective("Systeme")
        
        self.placeCadre(self.cadrePartie)
        
    def placePerspective(self,p="Cosmos"):
        self.pCourante = p
        self.perspectiveCourante.grid_forget()
        self.vaisseauSelection =[]
        self.cadreActions("")
        if p=="Systeme":
            self.batimentselection = None
            self.travailleurselection = []
            self.initSysteme()
            self.perspectiveCourante=self.perspectives[p]
            self.perspectiveCourante.grid(column=0,row=1,sticky=W+E+S+N)
            self.canevasCourant=self.canevas [p]
            
        elif p=="Planete":
            if self.planeteselection:
                if self.planeteselection.prop == self.parent.nom:
                    self.affichePlanete()
                    self.cadreInfoPlanete()
                    self.perspectiveCourante=self.perspectives[p]
                    self.perspectiveCourante.grid(column=0,row=1,sticky=W+E+S+N)
                    self.canevasCourant=self.canevas [p]
                else:
                    self.placePerspective("Systeme")
            else:
                self.placePerspective("Systeme")
        else:
            self.perspectiveCourante=self.perspectives[p]
            self.perspectiveCourante.grid(column=0,row=1,sticky=W+E+S+N)
            self.canevasCourant=self.canevas [p]
        
    def initCosmos(self,evt=""):
        self.canevasCosmos.delete(ALL)
        divx=self.x_espace/self.canevasCosmos.winfo_width()
        divy=self.y_espace/self.canevasCosmos.winfo_height()
        n=1
        for i in self.partie.etoiles:
            x=i.x/divx
            y=i.y/divy
            self.canevasCosmos.create_oval(x-n,y-n,x+n,y+n, fill="yellow",tags=("fond","etoile",i.id))
        self.afficheCosmos()
        
    def afficheCosmos(self):
        self.canevasCosmos.delete("miseajour")
        divx=self.x_espace/self.canevasCosmos.winfo_width()
        divy=self.y_espace/self.canevasCosmos.winfo_height()
        n=1
        j=self.partie.civs[self.moi]
        i=j.etoileMere
        x=i.x/divx
        y=i.y/divy
        self.canevasCosmos.create_oval(x-n,y-n,x+n,y+n, fill=j.couleur,tags=("miseajour","etoile",i.id,j.nom,"etoilemere"))
        n=3
        self.canevasCosmos.create_oval(x-n,y-n,x+n,y+n, outline=j.couleur,tags=("miseajour","etoile",i.id,j.nom,"etoilemere"))
            
    def afficheSysteme(self,i):
        self.canevasSysteme.delete(ALL)
        divx=self.canevasSysteme.winfo_width()/2
        divy=self.canevasSysteme.winfo_height()/2
        maxx=divx*2
        maxy=divy*2
        n=1
        for l in range(random.randrange(200)+50):
            x=random.randrange(divx*2)
            y=random.randrange(divy*2)
            self.canevasSysteme.create_oval(x,y,x+n,y+n,fill="white",tags=("pseudoetoiles",))
            
        n=20
        self.canevasSysteme.create_oval(divx-n,divy-n,divx+n,divy+n,fill="gold")
            
        for j in i.planetes:
            x=j.x
            y=j.y
            if j.terre:
                n=15
                couleur = self.modele.civs[j.prop].couleur #ici
                self.canevasSysteme.create_oval(divx+x-n,divy+y-n,divx+x+n,divy+y+n,outline="grey50",fill=couleur,tags=("planetes",j.id)) #ici
            else:
                n=10
                if j.prop != "" :
                    couleur = self.modele.civs[j.prop].couleur #ici
                    self.canevasSysteme.create_oval(divx+x-n,divy+y-n,divx+x+n,divy+y+n,outline="grey50",fill=couleur,tags=("planetes",j.id))
                else:
                    self.canevasSysteme.create_oval(divx+x-n,divy+y-n,divx+x+n,divy+y+n,outline="grey50",fill="grey",tags=("planetes",j.id))
        self.afficheVaisseaux()
        
    def afficheVaisseaux(self):
        self.canevasSysteme.delete("vaisseaux")
        self.canevasEspace.delete("vaisseaux")
        for i in self.modele.civs.keys():
            for k in self.modele.civs[i].flottes:
                if k.etoile == self.etoileselection:
                    divx=self.canevasSysteme.winfo_width()/2
                    divy=self.canevasSysteme.winfo_height()/2
                    x=k.x
                    y=k.y
                    self.canevasSysteme.create_oval(divx+x-3,divy+y-3,divx+x+3,divy+y+3,outline="grey50",fill=self.modele.civs[i].couleur,tags=("vaisseaux",k.id))
                elif k.etoile == None:
                    x=k.x*self.zoomFactor
                    y=k.y*self.zoomFactor 
                    self.canevasEspace.create_oval(x-3,y-3,x+3,y+3,outline="grey50",fill=self.modele.civs[i].couleur,tags=("vaisseaux",k.id))

    def verifierSiRessource(self,idBatiment):
        for batiment in self.planeteselection.listeBatiment:
            if int(idBatiment)==batiment.id:
                if self.planeteselection.caseList[batiment.case].ressources==0:
                    return False
                else:
                    return True
    
    def afficheRessources(self,food,wood,mineral,gas,science):
        self.labelBouffe.config(text="Nourriture : "+str(food))
        self.labelBois.config(text="Bois : "+str(wood))
        self.labelMetal.config(text="Mineraux : "+str(mineral))
        self.labelGaz.config(text="Gaz : "+str(gas))
        self.labelScience.config(text="Science : "+str(science))
    
    def affichePlanete(self):
        self.canevasPlanete.delete("batiment","travailleur","ressource","nbtravailleurs")
        if self.ligne == False:
            for i in range(2500):
                if i%50 == 0:
                    self.canevasPlanete.create_line(i,0,i,2500,width=1)
                    self.canevasPlanete.create_line(0,i,2500,i,width=1)
            self.ligne=True
        if self.etoileselection:
            for p in self.modele.etoiles[self.etoileselection.id].planetes:
                if p.id == self.planeteselection.id:
                    self.afficheRessources(p.listeRessource["food"],
                                           p.listeRessource["wood"],
                                           p.listeRessource["mineral"],
                                           p.listeRessource["gas"],
                                           p.listeRessource["science"])
                    for i in p.caseList.keys():
                        x,y = self.parsePosition(i)
                        x = x*50
                        y = y*50
                        if p.caseList[i].content:
                            id = p.caseList[i].batiment.id
                            nb = self.planeteselection.listeBatiment[id].nbTravailleur
                            if p.caseList[i].content == "Command Center":
                                self.canevasPlanete.create_rectangle(x,y,x+50,y+50,fill="gold",tags=("batiment","commandcenter",id))
                            if p.caseList[i].content == "vaisseau":
                                self.canevasPlanete.create_rectangle(x,y,x+50,y+50,fill="pink",tags=("batiment","vaisseaux",id))
                            elif p.caseList[i].content == "ferme":
                                if self.verifierSiRessource(id):
                                    self.canevasPlanete.create_rectangle(x,y,x+50,y+50,fill="red",tags=("batiment","ferme",id))
                                else:
                                    self.canevasPlanete.create_rectangle(x,y,x+50,y+50,fill="black",tags=("batiment","ferme",id))
                            elif p.caseList[i].content == "scierie":
                                if self.verifierSiRessource(id):
                                    self.canevasPlanete.create_rectangle(x,y,x+50,y+50,fill="saddle brown",tags=("batiment","scierie",id))
                                else:
                                    self.canevasPlanete.create_rectangle(x,y,x+50,y+50,fill="black",tags=("batiment","scierie",id))
                            elif p.caseList[i].content == "mine":
                                self.canevasPlanete.create_rectangle(x,y,x+50,y+50,fill="grey",tags=("batiment","mine",id))
                            elif p.caseList[i].content == "raffinerie":
                                self.canevasPlanete.create_rectangle(x,y,x+50,y+50,fill="aquamarine",tags=("batiment","raffinerie",id))
                            elif p.caseList[i].content == "manufacture":
                                self.canevasPlanete.create_rectangle(x,y,x+50,y+50,fill="orange",tags=("batiment","manufacture",id))
                            elif p.caseList[i].content == "tourelle":
                                self.canevasPlanete.create_rectangle(x,y,x+50,y+50,fill="purple",tags=("batiment","tourelle",id))
                            elif p.caseList[i].content == "laboratoire":
                                self.canevasPlanete.create_rectangle(x,y,x+50,y+50,fill="white",tags=("batiment","laboratoire",id))
                            self.canevasPlanete.create_text(x+50,y+50,anchor="se",text=str(nb),tags="nbtravailleurs")
                        elif p.caseList[i].nomRessource:
                            if p.caseList[i].nomRessource == "food":
                                self.canevasPlanete.create_oval(x,y,x+50,y+50,fill="red",tags=("food","ressource"))
                            elif p.caseList[i].nomRessource == "wood":
                                self.canevasPlanete.create_oval(x,y,x+50,y+50,fill="saddle brown",tags=("wood","ressource"))
                            elif p.caseList[i].nomRessource == "mineral":
                                self.canevasPlanete.create_oval(x,y,x+50,y+50,fill="grey",tags=("mineral","ressource"))
                            elif p.caseList[i].nomRessource == "gas":
                                self.canevasPlanete.create_oval(x,y,x+50,y+50,fill="aquamarine",tags=("gas","ressource"))
                     
                    for i in p.listeTravailleur:
                        if i.afficherTravailleur==True:
                            self.canevasPlanete.create_image(i.x,i.y,image=self.image,tags=("travailleur",i.id))
                            #self.canevasPlanete.create_oval(i.x-5,i.y-5,i.x+5,i.y+5,fill="cyan",tags=("travailleur",i.id))
                     
    def initEspace(self):
        self.afficheEspace()
        
    def afficheSelection(self):
        divx=self.canevasSysteme.winfo_width()/2
        divy=self.canevasSysteme.winfo_height()/2
        maxx=divx*2
        maxy=divy*2
        
        self.canevasSysteme.delete("vaisseauxSelection")
        self.canevasEspace.delete("vaisseauxSelection")
        n=7
            
        if self.pCourante == "Systeme":
            self.canevasSysteme.delete("vaisseauxSelection")
        else:
            self.canevasEspace.delete("vaisseauxSelection")
            
        for j in self.vaisseauxSelection:
            x=j.x
            y=j.y
            if self.pCourante == "Systeme":
                self.canevasSysteme.create_oval(divx+x-n,divy+y-n,divx+x+n,divy+y+n,outline="yellow",dash=( 2, 2 ), tags=("vaisseauxSelection"))
            else:
                x = j.x*self.zoomFactor
                y = j.y*self.zoomFactor
                self.canevasEspace.create_oval(x-n,y-n,x+n,y+n,outline="yellow",dash=( 2, 2 ), tags=("vaisseauxSelection"))
 
            
        self.canevasEspace.delete("etoileselection")
        if self.etoileselection:
            n=10*self.zoomFactor
            i=self.etoileselection
            x=i.x*self.zoomFactor
            y=i.y*self.zoomFactor
            self.canevasEspace.create_oval(x-n,y-n,x+n,y+n,outline="yellow",dash=( 2, 2 ),  tags=("etoileselection"))
        
        self.canevasSysteme.delete("planeteselection")
        if self.planeteselection :
            i = self.planeteselection
            n=25
            x=i.x
            y=i.y
            self.canevasSysteme.create_oval(divx+x-n,divy+y-n,divx+x+n,divy+y+n,outline="yellow",dash=( 2, 2 ),  tags=("planeteselection"))
 
        self.canevasPlanete.delete("travailleurselection")
        for travailleur in self.travailleurselection :
            if travailleur.afficherTravailleur == False:
                self.travailleurselection.remove(travailleur)
            else:
                dif=18
                i = travailleur
                self.canevasPlanete.create_oval(i.x-dif,i.y-dif,i.x+dif,i.y+dif,outline="yellow",dash=( 2, 2 ),  tags=("travailleurselection"))
                
        self.canevasPlanete.delete("batimentselection")
        if self.batimentselection:
            dif=35
            i = self.batimentselection
            self.canevasPlanete.create_oval(i.x+25-dif,i.y+25-dif,i.x+25+dif,i.y+25+dif,outline="yellow",dash=( 2, 2 ),  tags=("batimentselection"))
        
    def changeCible(self,selection,e):
        x=e.x
        y=e.y
        x1 = self.canevasEspace.canvasx(x)
        y1 = self.canevasEspace.canvasy(y)
        for i in selection:
            self.parent.changeCible(i.id,x1,y1)
            
    def creerServeur(self):
        nom=self.nomjoueur.get()
        leip=self.parent.monip
        if nom:
            pid=self.parent.creerServeur()
            if pid:
                self.demarreB.config(state=NORMAL)
                self.root.after(500,self.inscritClient)
        else:
            mb.showerror(title="Besoin d'un nom",message="Vous devez inscrire un nom pour vous connecter.")
            
                
    def inscritClient(self):
        nom=self.nomjoueur.get()
        leip=self.parent.monip
        self.parent.inscritClient(nom,leip)
        
    def connecterServeur(self):
        nom=self.nomjoueur.get()
        leip=self.autreip.get()
        if nom:
            self.parent.inscritClient(nom,leip)
    
    def afficheModele(self,civs):
        if self.perspective=="cosmos":
            return
        elif self.perspective=="espace":
            self.canevasEspace.delete("vaisseau")
            for i in civs.keys():
                for j in civs[i].objetsSpaciaux["vaisseau"]:
                    if moi:
                        x1=0
                        self.canevas.create_rectangle(moi.x-5+x1,moi.y-5,moi.x+5+x1,moi.y+5,fill="blue",tags=("vaisseau",))
                    if autre:
                        for i in autre:
                            self.canevas.create_rectangle(i.x-5,i.y-5,i.x+5,i.y+5,fill="lightgreen",tags=("vaisseau",))
                            self.canevas.create_text(i.x-5,i.y+5,text=i.nom,tags=("vaisseau",))
                
    def afficheEspace(self):
        self.canevasEspace.delete("etoiles")
        self.canevasEspace.delete("pseudoetoiles")
        n=3
        for i in self.partie.etoiles:
            n=i.taille/10*self.zoomFactor
            x=i.x*self.zoomFactor
            y=i.y*self.zoomFactor
            
            if i.etoileterre == True:
                couleur = self.modele.civs[i.prop].couleur
                self.canevasEspace.create_oval(x-n,y-n,x+n,y+n,fill=couleur,tags=("etoiles",i.id,"etoilemere"))
            else:  
                self.canevasEspace.create_oval(x-n,y-n,x+n,y+n,fill="yellow",tags=("etoiles",i.id))
        
        n=i.taille/100*self.zoomFactor
        for i in self.partie.pseudoetoiles:
            x=i[0]*self.zoomFactor
            y=i[1]*self.zoomFactor
            self.canevasEspace.create_oval(x,y,x+n,y+n,fill="white",tags=("pseudoetoiles",))
     
    def showPlanete(self):
        if self.perspective=="espace":
            self.placeCadre(self.cadrePlanete)
            self.perspective="planete"
            self.showB.config(text="Show Espace")
        else:
            self.placeCadre(self.ecranPartie)
            self.perspective="espace"
            self.showB.config(text="Show Planete")
        
    def centrerPlanete(self):
        self.centrerObjet( self.partie.civs[self.parent.nom].planeteMere.parent)
        
    def centrerObjet( self, obj):
        x=obj.x
        y=obj.y
        sx = float(self.x_espace)
        ecranx=float( self.canevasEspace.winfo_width() )/2.0
        posx = ( x-ecranx )/sx
        self.canevasEspace.xview( "moveto", posx )
        
        sy = float( self.y_espace )
        ecrany=float( self.canevasEspace.winfo_height() )/2.0
        posy = ( y-ecrany )/sy
        self.canevasEspace.yview( "moveto", posy )
        
    def ecrireMessage(self,message,nom):
        self.chatText.config(state=NORMAL)
        self.chatText.insert(END, '\n')
        self.chatText.insert(END, "[JOUEUR]"+nom+": ")
        self.chatText.insert(END, '\n')
        self.chatText.insert(END, message)
        self.chatText.yview(END)
        self.chatText.config(state=DISABLED)
        
    def envoyerMessage(self,event):
        message = self.inputText.get()
        if message == "/ip":
            self.ecrireIP()
        else:
            self.parent.envoyerMessage(message)
        self.inputText.delete(0,END)
    
    def ecrireIP(self):
        self.chatText.config(state=NORMAL)
        self.chatText.insert(END, '\n')
        self.chatText.insert(END, "IP est : "+self.parent.monip)
        self.chatText.yview(END)
        self.chatText.config(state=DISABLED)
        
    def showLinkedPlanet(self,planete):
        self.etoileselection = planete.parent
        self.planeteselection = planete
        self.placePerspective("Planete")

    def showLinkedSystem(self,etoile):
        self.etoileselection = etoile
        self.planeteselection = etoile.planetes[0]
        self.placePerspective("Systeme")
        
    def ecrireStatus(self,source,system,planet,message):
        if self.parent.nom == planet.prop:
            self.chatText.config(state=NORMAL)
            self.chatText.insert(END, '\n')
            self.chatText.insert(END, "["+source+"]")
            self.chatText.insert(END, "["+str(system.id)+"]", self.hyperlink.add(lambda:self.showLinkedSystem(system)))
            self.chatText.insert(END, "["+str(planet.id)+"]", self.hyperlink.add(lambda:self.showLinkedPlanet(planet)))
            self.chatText.insert(END, ": ")
            self.chatText.insert(END, '\n')
            #self.chatText.insert(END, "["+str(datetime.now())+"]"+" "+prefix+message)
            self.chatText.insert(END, message)
            self.chatText.config
            self.chatText.yview(END)
            self.chatText.config(state=DISABLED)
    
    def ecrireSystemReport(self,source,message):
        self.chatText.config(state=NORMAL)
        self.chatText.insert(END, '\n')
        self.chatText.insert(END, "["+source+"]")
        self.chatText.insert(END, ": ")
        self.chatText.insert(END, '\n')
        self.chatText.insert(END, message)
        self.chatText.config
        self.chatText.yview(END)
        self.chatText.config(state=DISABLED)  

    def construireTravailleur(self,event):
        if self.planeteselection.CC.creerTravailleurPossible():
            self.parent.construireTravailleur(self.planeteselection)
            
    def construireFerme(self,event):
        self.travailleurselection[0].batimentConstruire="ferme"
    
    def construireScierie(self,event):
        self.travailleurselection[0].batimentConstruire="scierie"
    
    def construireMine(self,event):
        self.travailleurselection[0].batimentConstruire="mine"

    def construireRaffinerie(self,event):
        self.travailleurselection[0].batimentConstruire="raffinerie"
        
    def construireManufacture(self,event):
        self.travailleurselection[0].batimentConstruire="manufacture"
        
    def construireTourelle(self,event):
        self.travailleurselection[0].batimentConstruire="tourelle"
        
    def construireLaboratoire(self,event):
        self.travailleurselection[0].batimentConstruire="laboratoire"

    def donnerCible(self,event):
        for i in self.travailleurselection:
            if i.batimentAffecter == "":
                i.cible=[]
                i.cible.append(self.canevasPlanete.canvasx(event.x))
                i.cible.append(self.canevasPlanete.canvasy(event.y))
                t=self.canevasCourant.gettags(CURRENT)
                if t:
                    if t[0]=="batiment":
                        i.batimentAffecter=t[2]
    
    def donnerCibleVaisseaux(self,event):
        self.vaisseauxselectionid = []
        for v in self.vaisseauxSelection:
            self.vaisseauxselectionid.append(v.id)
            t=self.canevasCourant.gettags(CURRENT)
            v.etoileAffecter = None
            v.planeteAffecter = None
            if t:
                if t[0]=="etoiles":
                    print("Affectation Etoile", t[1])
                    v.etoileAffecter=t[1]
                elif t[0]=="planetes":
                    print("Affectation Planete", t[1])
                    v.planeteAffecter = t[1]
                   
                    
        divx=self.canevasSysteme.winfo_width()/2
        divy=self.canevasSysteme.winfo_height()/2
        self.parent.donnerCibleVaisseau(self.vaisseauxselectionid,event,divx,divy)         
        
    def retirerTravailleur(self,event):
        self.parent.retirerTravailleur(self.planeteselection,self.batimentselection)
        
    def doubleClic(self,event=None):
        t=self.canevasCourant.gettags(CURRENT)
        if t:
            if t[0]=="planetes":
                print("Entrer planete")
                self.perspective="Planete"
                self.placePerspective("Planete")
            elif t[0]=="etoiles":
                print("Entrer systeme")
                self.perspective="Systeme"
                self.placePerspective("Systeme")
            elif t[1] == "batiment":
                i.batimentAffecter = t[2]
                
    def construireVaisseau(self,event):
        if self.planeteselection :
            i=self.planeteselection
            self.parent.creerVaisseau(self.planeteselection) 
            self.afficheVaisseaux()
            
            
    def dessinerRectangle(self,evt):
        self.canevasPlanete.delete("rectSelection")
        if self.rectStartX:
            self.rectX = evt.x
            self.rectY = evt.y
            scrollx = self.sx.get()
            scrolly = self.sy.get()
            self.offsetX = 2500*scrollx[0]
            self.offsetY = 2500*scrolly[0]
            self.canevasPlanete.create_rectangle(self.rectStartX+self.offsetX,self.rectStartY+self.offsetY,evt.x+self.offsetX,evt.y+self.offsetY,width=3,dash=(100,2),outline="yellow",tags="rectSelection")
        else:
            self.rectStartX = evt.x
            self.rectStartY = evt.y
            self.canevasPlanete.bind("<ButtonRelease-1>",self.selectBoxTravailleurs)
    
    def dessinerRectangleSysteme(self,evt):
        divx=self.canevasSysteme.winfo_width()/2
        divy=self.canevasSysteme.winfo_height()/2
        self.canevasSysteme.delete("rectSelection")
        if self.rectStartX:
            self.rectX = evt.x
            self.rectY = evt.y
            self.offsetX = 0
            self.offsetY = 0
            self.canevasSysteme.create_rectangle(self.rectStartX+self.offsetX,self.rectStartY+self.offsetY,evt.x+self.offsetX,evt.y+self.offsetY,width=3,dash=(100,2),outline="yellow",tags="rectSelection")
        else:
            self.rectStartX = evt.x
            self.rectStartY = evt.y
            self.canevasSysteme.bind("<ButtonRelease-1>",self.selectBoxVaisseaux)
            
    def dessinerRectangleEspace(self,evt):
        divx=self.canevasEspace.winfo_width()/2
        divy=self.canevasEspace.winfo_height()/2
        self.canevasEspace.delete("rectSelection")
        if self.rectStartX:
            self.rectX = evt.x
            self.rectY = evt.y
            scrollx = self.sxespace.get()
            scrolly = self.syespace.get()
            self.offsetX = self.modele.gameSettings["x_espace"]*scrollx[0]
            self.offsetY = self.modele.gameSettings["y_espace"]*scrolly[0]
            self.canevasEspace.create_rectangle(self.rectStartX+self.offsetX,self.rectStartY+self.offsetY,evt.x+self.offsetX,evt.y+self.offsetY,width=3,dash=(100,2),outline="yellow",tags="rectSelection")
        else:
            self.rectStartX = evt.x
            self.rectStartY = evt.y
            self.canevasEspace.bind("<ButtonRelease-1>",self.selectBoxVaisseauxEspace)
            
    def selectBoxVaisseaux(self,evt):
        divx=self.canevasSysteme.winfo_width()/2
        divy=self.canevasSysteme.winfo_height()/2
        if self.rectStartX:
            self.vaisseauxSelection=[]
            self.planeteselection=None
            self.canevasSysteme.delete("rectSelection")
            try:
                minX = self.rectStartX
                if  minX > self.rectX:
                    minX = self.rectX-divx
                    maxX = self.rectStartX-divx
                else:
                    maxX = self.rectX-divx
                    minX = self.rectStartX-divx
                    
                minY = self.rectStartY
                if  minY > self.rectY:
                    maxY = self.rectStartY-divy
                    minY = self.rectY-divy
                else:
                    minY = self.rectStartY-divy
                    maxY = self.rectY-divy
            except TypeError:
                maxX = minX = self.rectStartX-divx
                maxY = minY = self.rectStartY-divy
                
            for i in self.modele.civs[self.parent.nom].flottes:
                if minX<i.x and maxX>i.x and minY<i.y and maxY>i.y:
                    self.vaisseauxSelection.append(i)
                    self.cadreActions("vaisseaux")
            self.rectStartX=None
            self.rectStartY=None
            self.afficheSelection()
    
    def selectBoxVaisseauxEspace(self,evt):
        divx=self.canevasEspace.winfo_width()/2
        divy=self.canevasEspace.winfo_height()/2
        if self.rectStartX:
            self.vaisseauxSelection=[]
            self.planeteselection=None
            self.canevasEspace.delete("rectSelection")
            minX = self.rectStartX
            try:
                minX = self.rectStartX
                if  minX > self.rectX:
                    minX = self.rectX+self.offsetX
                    maxX = self.rectStartX+self.offsetX
                else:
                    maxX = self.rectX+self.offsetX
                    minX = self.rectStartX+self.offsetX
                    
                minY = self.rectStartY
                if  minY > self.rectY:
                    maxY = self.rectStartY+self.offsetY
                    minY = self.rectY+self.offsetY
                else:
                    minY = self.rectStartY+self.offsetY
                    maxY = self.rectY+self.offsetY
            except TypeError:
                maxX = minX = self.rectStartX+self.offsetX
                maxY = minY = self.rectStartY+self.offsetY
                
            for i in self.modele.civs[self.parent.nom].flottes:
                if minX<i.x and maxX>i.x and minY<i.y and maxY>i.y and i.etoile == None:
                    self.vaisseauxSelection.append(i)
                    self.cadreActions("vaisseaux")
            self.rectStartX=None
            self.rectStartY=None
            self.afficheSelection()
    
    def selectBoxTravailleurs(self,evt):
        if self.rectStartX:
            self.travailleurselection=[]
            self.batimentselection=None
            self.canevasPlanete.delete("rectSelection")
            try:
                minX = self.rectStartX
                if  minX > self.rectX:
                    minX = self.rectX+self.offsetX
                    maxX = self.rectStartX+self.offsetX
                else:
                    maxX = self.rectX+self.offsetX
                    minX = self.rectStartX+self.offsetX
                    
                minY = self.rectStartY
                if  minY > self.rectY:
                    maxY = self.rectStartY+self.offsetY
                    minY = self.rectY+self.offsetY
                else:
                    minY = self.rectStartY+self.offsetY
                    maxY = self.rectY+self.offsetY
            except TypeError:
                maxX = minX = self.rectStartX+self.offsetX
                maxY = minY = self.rectStartY+self.offsetY
                
                
            for travailleur in self.planeteselection.listeTravailleur:
                if minX<travailleur.x and maxX>travailleur.x and minY<travailleur.y and maxY>travailleur.y:
                    self.travailleurselection.append(travailleur)
                    self.cadreActions("travailleur")
            self.rectStartX=None
            self.rectStartY=None
            self.afficheSelection()
    
    def warpOutVaisseaux(self,evt): #### Pour le warp-in voir donnerCibleVaisseaux() et Flottes.DeplacerVaisseaux()
        x,y = None,None
        listevaisseauxid = []
        if self.vaisseauxSelection:
            for vaisseau in self.vaisseauxSelection:
                listevaisseauxid.append(vaisseau.id)
            self.parent.warpOut(listevaisseauxid)
                
    def decollerVaisseau(self,evt):
        self.parent.creerVaisseauCargo(self.batimentselection.parent.id,self.batimentselection.id)

    def ajouterBouffeVaisseaux(self,evt):
        self.batimentselection.AffecterRessources("Bouffe")
    
    def ajouterBoisVaisseaux(self,evt):
        self.batimentselection.AffecterRessources("Bois")
    
    def ajouterMineraiVaisseau(self,evt):
        self.batimentselection.AffecterRessources("Minerai")
    
    def ajouterGazVaisseau(self,evt):
        self.batimentselection.AffecterRessources("Gaz")
