import random
import math
from helper import Helper

from Client import *
from Modele import *
from Entites import *
import math
# Commentaire

class Batiment():
    def __init__(self,parent):
        self.parent=parent
        self.case=None
        self.x=None
        self.y=None
        self.surface=0 #WTF IS THAT
        self.nbTravailleur=0
        self.typeBatiment="" # ajoute mais pas dans le CRC 
        # dont {"production","ressource","defense","laboratoire","CC"}
        self.tacheBatiment=[]       
        self.LimiteTravailleurTolere=3
        self.id=0
        self.niveauTechno = 0
        
            
#    def NombreTravailleurPresent(self):         
#        return len(self.listeTravailleursBatiment)
#     
    def AffectationTravailleur(self):
        self.nbTravailleur+=1         
       
    def RecevoirTravailleurPossible(self, batiment):# savoir si peut recevoir un travailleur dans le batiment
     if 1 + batiment.nbTravailleur  < batiment.LimiteTravailleurTolere:
          return True
     else:
          return False
      
    def VerifierSiConstructionPossible(self): # verification si la construction est possible
    # si PositionSelectionner n'est pas occupe et si la terre corespont au type de batiment
    # return true
    # else
    # return false
        pass
    
    def DetruireBatiment(self): # detruit un batiment
        
        pass
    
    def RetirerTravailleur(self): # retire travailleur d'un batiment
        for travailleur in self.parent.listeTravailleur:
            print("Batiment affecter",travailleur.batimentAffecter)
            print("ID",self.id)
            if travailleur.batimentAffecter:
                if int(travailleur.batimentAffecter) == int(self.id) and self.nbTravailleur>0:
                    travailleur.afficherTravailleur=True
                    travailleur.batimentAffecter=""
                    self.nbTravailleur-=1
                    if isinstance(self, VaisseauSurface):
                        self.travailleurs.remove(travailleur)
                    print("retirer travailleur", self.id, travailleur.afficherTravailleur)
                    break
    
    def ListeDeConstructionsAenvoyer(self):     # liste de construction a envoyer au serveur
        pass
 
class VaisseauSurface(Batiment):
    def __init__(self,parent):
        Batiment.__init__(self,parent)         
        self.typeBatiment="vaisseau"
        self.maxRessources = 500
        self.travailleurs = []
        self.ressources = {"Bois":0,
                           "Bouffe":0,
                           "Minerai":0,
                           "Gaz":0}
        
    def AffecterRessources(self,ressource):
        totalRessources = 0
        for nom in self.ressources.keys():
            totalRessources = totalRessources + self.ressources[nom]
        if ressource == "Bois":
            nom = "wood"
        elif ressource == "Bouffe":
            nom = "food"
        elif ressource == "Minerai":
            nom = "mineral"
        elif ressource == "Gaz":
            nom = "gas"
        if totalRessources < self.maxRessources and self.parent.listeRessource[nom]>=100:
            self.parent.listeRessource[nom] -= 100
            self.ressources[ressource] += 100
    
    def AffectationTravailleur(self,travailleur):
        self.nbTravailleur+=1
        self.travailleurs.append(travailleur)
        print(self.typeBatiment, self.nbTravailleur) 
    
        
class BatimentRessource(Batiment):
    def __init__(self,parent,ressource):
        Batiment.__init__(self,parent)        
        self.prod = 5
        self.typeRessource= ressource  
        self.typeBatiment="ressource"
        
    def calculRessource(self):
        retrait = (self.nbTravailleur*self.prod)
        if self.parent.caseList[self.case].ressources > 0:
            if self.parent.caseList[self.case].ressources < retrait:
                retrait = self.parent.caseList[self.case].ressources

            self.parent.caseList[self.case].ressources-=retrait
            self.parent.ressources[self.typeRessource]-=retrait
            self.parent.listeRessource[self.typeRessource]+=retrait
             
class BatimentProduction(Batiment):
    def __init__(self,parent):
        Batiment.__init__(self,parent)    
        self.specialisation=[]
        self.typeBatiment="production"
              
class CommandCenter(Batiment):
    def __init__(self,parent):
        Batiment.__init__(self,parent)
        self.specialisation=[]
        self.surface = 10
        self.parent=parent
        self.idTravailleur=0
        self.typeBatiment="command"
        
    def creerTravailleurPossible(self):
        if self.parent.listeRessource["food"] >= 100:
            return True
        else:
            return False
             
    def creerTravailleur(self):
        def CostProcess():
            ok = True
            listeRessourceTemp = self.parent.listeRessource.copy()
            listeRessourceTemp["food"] -= 50
            if listeRessourceTemp["food"] < 0:
                    ok = False
            if ok :
                self.parent.listeRessource = listeRessourceTemp.copy()
                return True
            else:
                return False
        if CostProcess():
            self.parent.nbrTravailleursCreer += 1
            self.parent.listeTravailleur.append(Travailleur(self,self.idTravailleur))
            t = self.parent.listeTravailleur[self.idTravailleur-1]
            t.x=1300
            t.y=1400
            self.placementTravailleur(t)
            self.idTravailleur+=1
    
    def creerTravailleurColonisation(self):
        self.parent.nbrTravailleursCreer += 1
        self.parent.listeTravailleur.append(Travailleur(self,self.idTravailleur))
        if len(self.parent.listeTravailleur)>1:
            index = len(self.parent.listeTravailleur)
        else:
            index = 1
        t = self.parent.listeTravailleur[index-1]
        t.x=1300
        t.y=1400
        self.placementTravailleur(t)
        self.idTravailleur+=1   
                          
    def placementTravailleur(self,t):
        correct = True
        for i in self.parent.listeTravailleur:
            if t.id != i.id:
                if t.x == i.x and t.y == i.y:
                    n=random.randrange(4)
                    deplacement=15
                    if n<=1:
                        t.x+=deplacement
                        t.y+=deplacement
                    elif n>1 and n<=2:
                        t.x+=deplacement
                    elif n>2 and n<3:
                        t.x-=deplacement
                    else:
                        t.x-=deplacement
                        t.y-=deplacement
                    correct=False
        if correct == False:
            self.placementTravailleur(t)
            
    def ameliorerBatiment(self,type,planete):
        def CostProcess(niveau):
            science = planete.listeRessource["science"]
            if science >= 100*niveau:
                return True
            else:
                return False

        if type == "Ferme":
            if self.parent.listeFerme != []:
                if CostProcess(self.parent.listeFerme[0].niveauTechno):
                    planete.listeRessource["science"] -= 100*self.parent.listeFerme[0].niveauTechno
                    for i in self.parent.listeFerme:
                        i.LimiteTravailleurTolere += 1
                        i.niveauTechno += 1
        elif type == "Scierie":
            if self.parent.listeScierie != []:
                if CostProcess(self.parent.listeScierie[0].niveauTechno):
                    planete.listeRessource["science"] -= 100*self.parent.listeScierie[0].niveauTechno
                    for i in self.parent.listeScierie:
                        i.LimiteTravailleurTolere += 1
                        i.niveauTechno += 1
        elif type == "Mine":
            if self.parent.listeMine != []:
                if CostProcess(self.parent.listeMine[0].niveauTechno):
                    planete.listeRessource["science"] -= 100*self.parent.listeMine[0].niveauTechno
                    for i in self.parent.listeMine:
                        i.LimiteTravailleurTolere += 1
                        i.niveauTechno += 1
        elif type == "Raffinerie":
            if self.parent.listeRaffinerie != []:
                if CostProcess(self.parent.listeRaffinerie[0].niveauTechno):
                    planete.listeRessource["science"] -= 100*self.parent.listeRaffinerie[0].niveauTechno
                    for i in self.parent.listeRaffinerie:
                        i.LimiteTravailleurTolere += 1
                        i.niveauTechno += 1
        elif type == "Laboratoire":
            if self.parent.listeLabo != []:
                if CostProcess(self.parent.listeLabo[0].niveauTechno):
                    planete.listeRessource["science"] -= 100*self.parent.listeLabo[0].niveauTechno
                    for i in self.parent.listeLabo:
                        i.LimiteTravailleurTolere += 1
                        i.niveauTechno += 1
        elif type == "Manufacture":
            if self.parent.listeManufacture != []:
                if CostProcess(self.parent.listeManufacture[0].niveauTechno):
                    planete.listeRessource["science"] -= 100*self.parent.listeManufacture[0].niveauTechno
                    for i in self.parent.listeManufacture:
                        i.LimiteTravailleurTolere += 1
                        i.niveauTechno += 1
        elif type == "Tourelle":
            if self.parent.listeTourelle != []:
                if CostProcess(self.parent.listeTourelle[0].niveauTechno):
                    planete.listeRessource["science"] -= 100*self.parent.listeTourelle[0].niveauTechno
                    for i in self.parent.listeTourelle:
                        i.LimiteTravailleurTolere += 1
                        i.niveauTechno += 1
        
class Laboratoire(Batiment):
    def __init__(self,parent):
        Batiment.__init__(self,parent)
        self.prod = 1
        self.typeBatiment = "science"
    
    def calculRessource(self):
        ajout = self.nbTravailleur*self.prod
        self.parent.listeRessource["science"]+=ajout
        
class Station(object):
    def __init__(self,parent,id):
        self.parent=parent
        self.id=id
        self.nom=parent.nom
        dirx=random.randrange(2)-1
        if dirx==0:
            dirx=1
        diry=random.randrange(2)-1
        if diry==0:
            diry=1
        self.x=(random.randrange(10)+10*dirx)+parent.planeteMere.parent.x
        self.y=(random.randrange(10)+10*diry)+parent.planeteMere.parent.y
        print("STATION",self.x,self.y)
        
    def prochaineAction(self):
        pass
                
class Tourelle(Batiment):
    def __init__(self,parent):
        Batiment.__init__(self,parent)
        self.power = 1
        self.specialization = None
        self.attackSpeed = 1
        self.typeBatiment = "combat"
    
    def calculDmg(self):
        power = (self.nbTravailleur*self.power)
        return power

class Manufacture(BatimentProduction):
	def __init__(self):
		pass
	
	def buildCargoo(self):
		pass
		Others.VaisseauCargo(x,y,z,techLevel)
	
	def buildScout(self):
		pass
		Others.VaisseauExploration(x,y,z,techLevel)
		
	def buildCombat(self):
		pass
		Others.VaisseauCombat(x,y,z,techLevel)

class Travailleur():
    def __init__(self,parent,id): #ici
        self.parent=parent
        self.client = parent.parent.parent.parent.parent
        self.afficherTravailleur=True
        self.x=0
        self.y=0
        self.vitesse=10
        self.tacheTravailleur=None # Taches que le travailleur possede
        self.BatimentAttribuer=None # batiments que le joueur posssede
        self.upkeep=0
        self.id=id          
        self.NombreAEnvoyer=0
        self.BatimentChoisit=""
        self.ListeEnvoieTravailleur=[]
        self.cible=[]
        self.batimentConstruire=""
        self.batimentAffecter=""
    
    def ConstructionPossible(self,planete,x,y,ressource): # verification si la construction est possible
        if ressource:
            if planete.caseList[str(int(x))+","+str(int(y))].content == None and planete.caseList[str(int(x))+","+str(int(y))].nomRessource == ressource :
                return True
            else:
                return False
        else:
            if planete.caseList[str(int(x))+","+str(int(y))].content == None and planete.caseList[str(int(x))+","+str(int(y))].nomRessource == "" :
                return True
            else:
                return False
        idPlanete,nomRessource,caseArriveeX,caseArriveeY,nomBatiment
        
    def ConstruireBatiment(self,planete,batiment,posX,posY,nomBatiment): #construit batiment
        def CostProcess():
            ok = True
            listeRessourceTemp = planete.listeRessource.copy()
            if nomBatiment == "scierie":
                listeRessourceTemp["mineral"] -= 150
                listeRessourceTemp["food"] -= 100
            elif nomBatiment == "mine":
                listeRessourceTemp["wood"] -= 100
                listeRessourceTemp["food"] -= 150
            elif nomBatiment == "ferme":
                listeRessourceTemp["wood"] -= 200
                listeRessourceTemp["mineral"] -= 50
            elif nomBatiment == "raffinerie":
                listeRessourceTemp["wood"] -= 100
                listeRessourceTemp["mineral"] -= 100
                listeRessourceTemp["food"] -= 100
            elif nomBatiment == "manufacture":
                listeRessourceTemp["wood"] -= 200
                listeRessourceTemp["mineral"] -= 200
                listeRessourceTemp["food"] -= 150
            elif nomBatiment == "laboratoire":
                listeRessourceTemp["wood"] -= 500
                listeRessourceTemp["mineral"] -= 500
                listeRessourceTemp["food"] -= 250
            elif nomBatiment == "tourelle":
                listeRessourceTemp["wood"] -= 250
                listeRessourceTemp["mineral"] -= 250
                listeRessourceTemp["food"] -= 150
                listeRessourceTemp["gas"] -= 50
                
            for ressource in listeRessourceTemp:
                if listeRessourceTemp[ressource] < 0:
                    ok = False
            if ok :
                planete.listeRessource = listeRessourceTemp.copy()
                return True
            else:
                self.parent.parent.parent.parent.sendStatusReport("TRAVAILLEUR", self.parent.parent.parent, self.parent.parent,"Ressources insuffisantes")
                return False
            
        if nomBatiment == "scierie":
            ressource = "wood"
        elif nomBatiment == "mine":
            ressource = "mineral"
        elif nomBatiment == "ferme":
            ressource = "food"
        elif nomBatiment == "raffinerie":
            ressource = "gas"
        else:
            ressource = None
        test = self.ConstructionPossible(planete,posX,posY,ressource)
        print(test)
        
        if test:
            if CostProcess():
                batiment.case=str(int(posX))+","+str(int(posY))
                batiment.x=posX*50
                batiment.y=posY*50
                planete.caseList[str(int(posX))+","+str(int(posY))].content = nomBatiment
                batiment.id = planete.idBatiment
                planete.listeBatiment.append(batiment)
                planete.idBatiment+=1
                planete.caseList[batiment.case].batiment=batiment
                if nomBatiment == "scierie":
                    planete.listeScierie.append(batiment)
                elif nomBatiment == "mine":
                    planete.listeMine.append(batiment)
                elif nomBatiment == "ferme":
                    planete.listeFerme.append(batiment)
                elif nomBatiment == "raffinerie":
                    planete.listeRaffinerie.append(batiment)
                elif nomBatiment == "manufacture":
                    planete.possibiliteVaisseau = True
                    planete.listeManufacture.append(batiment)
                elif nomBatiment == "laboratoire":
                    planete.listeLabo.append(batiment)
                elif nomBatiment == "tourelle":
                    planete.listeTourelle.append(batiment)
        else:
            self.parent.parent.parent.parent.sendStatusReport("TRAVAILLEUR", self.parent.parent.parent, self.parent.parent,"Emplacement invalide")
             
    def AssignerTravailleur(self):
        for i in self.parent.parent.listeBatiment:
            if self.batimentAffecter:
                if int(self.batimentAffecter) == int(i.id):
                    if i.nbTravailleur < i.LimiteTravailleurTolere and not isinstance(i, VaisseauSurface):
                        i.AffectationTravailleur()
                        self.afficherTravailleur=False 
                    elif i.nbTravailleur < i.LimiteTravailleurTolere and isinstance(i, VaisseauSurface):
                        print("Vaisseau")
                        self.parent = i
                        i.AffectationTravailleur(self)
                        self.afficherTravailleur=False                    
                    else:  
                        self.batimentAffecter = ""
                        print("batiment plein")            
        # ceci est un model, parce que je sais qu on va travailler avec des cases
                
    def EnvoieListeTravailleur(self): # retourne l'assignation du travailleur au un batiment
        return ListeEnvoieTravailleur

    def assignerTravailleur(self,batiment):
        self.batimentAffecter = batiment
        self.AssignerTravailleur()
        
    def DeplacerTravailleur(self,departX,departY,arriveeX,arriveeY):
        angle = Helper.calcAngle(departX,departY,arriveeX,arriveeY)         # deplacement du travailleur
        x,y=Helper.getAngledPoint(angle,self.vitesse,departX,departY)

        self.x=x
        self.y=y
        
        d=Helper.calcDistance(self.x,self.y,arriveeX,arriveeY)
        caseArriveeX=((arriveeX-arriveeX%50)/50)
        caseArriveeY=((arriveeY-arriveeY%50)/50)
        caseArrivee = self.parent.parent.caseList[str(int(caseArriveeX))+","+str(int(caseArriveeY))]
        for i in self.parent.parent.listeBatiment:
            if caseArrivee==i.case:
                i.listeTravailleurBatiment = self
#        if caseArrivee.batiment != None:
#            caseArrivee.batiment.listeTravailleursBatiment = self
#            print("NEW TRAVAILLEUR YAYAYAYAYAYAYAYAYAYAY")
        if d<self.vitesse:
            self.cible=[]
            if self.batimentAffecter!="":
                self.parent.parent.parent.parent.parent.assignerTravailleur(self.parent.parent,self,self.batimentAffecter)
            if self.batimentConstruire: 
                print("Construction",caseArriveeX,caseArriveeY,self.batimentConstruire)
                if self.batimentConstruire == "ferme":
                    self.client.construireBatimentRessource(self.parent.parent,"food",caseArriveeX,caseArriveeY,"ferme")   
                elif self.batimentConstruire == "scierie":
                    self.client.construireBatimentRessource(self.parent.parent,"wood",caseArriveeX,caseArriveeY,"scierie")
                    #self.ConstruireBatiment(self.parent.parent,BatimentRessource(self.parent.parent,"wood"),caseArriveeX,caseArriveeY,"scierie")
                elif self.batimentConstruire == "mine":
                    self.client.construireBatimentRessource(self.parent.parent,"mineral",caseArriveeX,caseArriveeY,"mine")
                elif self.batimentConstruire == "raffinerie":
                    self.client.construireBatimentRessource(self.parent.parent,"gas",caseArriveeX,caseArriveeY,"raffinerie")
                elif self.batimentConstruire == "manufacture":
                    self.client.construireBatimentProduction(self.parent.parent,caseArriveeX,caseArriveeY,"manufacture")
                elif self.batimentConstruire == "tourelle":
                    self.client.construireTourelle(self.parent.parent,caseArriveeX,caseArriveeY,"tourelle")
                else:
                    self.client.construireLaboratoire(self.parent.parent,caseArriveeX,caseArriveeY,"laboratoire")
                self.batimentConstruire=""
    
    def ScanFogWar(self):        
        pass
        DeplacerTravailleur()
