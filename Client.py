# -*- encoding: ISO-8859-1 -*-
import Pyro4
import random
from subprocess import Popen
import os
import socket
import platform

from Modele import *
from Vue import *
from helper import Helper

class Controleur(object):
    def __init__(self):
        self.nom=""
        self.cadre=0
        self.actions=[]
        self.serveurLocal=0
        self.serveur=0 
        s = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
        s.connect(("gmail.com",80))
        self.monip=s.getsockname()[0]
        s.close()
        self.modele = Modele(self)
        self.vue = Vue(self,300,200,self.modele.gameSettings["x_espace"],self.modele.gameSettings["y_espace"])
        
    def creerServeur(self):
        if platform.python_version_tuple()[0]=='3':
            p="python3"
        else:
            p="python"
        cwd=os.getcwd()
        testJMServeur=cwd+"\\"+"Serveur.py"
        #pid = Popen([p, testJMServeur]).pid
        pid = Popen(["C:\\Python32\\Python.exe", "Serveur.py"]).pid
        self.serveurLocal=1
        return pid
        
    def jeQuitte(self):
        if self.serveur:
            self.serveur.jeQuitte(self.nom)
        
    def stopServeur(self):
        rep=self.serveur.quitter()
        self.serveur=0
        input("FERMER")
        
    def inscritClient(self,nom,leip):
        ad="PYRO:controleurServeur@"+leip+":54440"
        self.serveur=Pyro4.core.Proxy(ad)
        Pyro4.socketutil.setReuseAddr(self.serveur)
        
        rep=self.serveur.inscritClient(nom)
        if rep[0]:
            self.modele.rdseed=rep[2]
            random.seed(self.modele.rdseed)
            self.nom=nom
            #self.vue.canevasEspace.bind("<Button>",self.vue.changeCible)
            self.vue.afficheAttente()
            self.timerAttend()
        else:
            print("NON inscrit: recommencer avec un autre nom !")
        
    def demarrePartie(self):
        rep=self.serveur.demarrePartie()
                
    def changeCible(self,monid,x,y):
        self.actions.append([self.nom,"changeCible",[monid,x,y]])
        
    def envoyerMessage(self,message):
        self.actions.append([self.nom,"message",message])
        
    def construireBatimentRessource(self,planete,ressource,caseArriveeX,caseArriveeY,nomBatiment):
        self.actions.append([self.nom,"construireBatimentRessource",[planete.id,ressource,caseArriveeX,caseArriveeY,nomBatiment]])
    
    def construireBatimentProduction(self,planete,caseArriveeX,caseArriveeY,nomBatiment):
        self.actions.append([self.nom,"construireBatimentProduction",[planete.id,caseArriveeX,caseArriveeY,nomBatiment]])
        
    def construireTourelle(self,planete,caseArriveeX,caseArriveeY,nomBatiment):
        self.actions.append([self.nom,"construireTourelle",[planete.id,caseArriveeX,caseArriveeY,nomBatiment]])
        
    def construireLaboratoire(self,planete,caseArriveeX,caseArriveeY,nomBatiment):
        self.actions.append([self.nom,"construireLaboratoire",[planete.id,caseArriveeX,caseArriveeY,nomBatiment]])
    
    def construireTravailleur(self,planete):
        self.actions.append([self.nom,"construireTravailleur",[planete.id]])
        
    def assignerTravailleur(self,planete,travailleur,idBatiment):
         self.actions.append([self.nom,"assignerTravailleur",[planete.id,travailleur.id,idBatiment]])
    
    def retirerTravailleur(self,planete,batiment):
         self.actions.append([self.nom,"retirerTravailleur",[planete.id,batiment.id]])
         
    def creerVaisseau(self,planete):
        self.actions.append([self.nom,"creerVaisseau",[planete.id]])
        
    def creerVaisseauCargo(self,planeteId,batimentId):
        self.actions.append([self.nom,"creerVaisseauCargo",[planeteId,batimentId]])
        
    def donnerCibleVaisseau(self,selection,event,divx,divy):
        self.actions.append([self.nom,"donnerCibleVaisseau",[selection,event.x,event.y,divx,divy]])
    
    def warpOut(self,selection):
        self.actions.append([self.nom,"warpOut",[selection]])
    
    def warpIn(self,vaisseau):
        self.actions.append([self.nom,"warpIn",[vaisseau.id,vaisseau.etoileAffecter]])
        
    def atterirVaisseau(self,vaisseau):
        self.actions.append([self.nom,"atterirVaisseau",[vaisseau.id,vaisseau.planeteAffecter]])
        
    def conquetePlanete(self,vaisseau):
        self.actions.append([self.nom,"conquetePlanete",[vaisseau.id,vaisseau.planeteAffecter]])
        
    def ameliorerBatiment(self,type,planete):
        self.actions.append([self.nom,"ameliorerBatiment",[type,planete]])
        
    def traite(self,listeTraite):
        self.actions.append([self.nom,"traite",[listeTraite]])
    # ******  SECTION d'appels automatique        
    def timerAttend(self):
        if self.serveur:
            rep=self.serveur.faitAction([self.nom,self.cadre,[]])
            if rep[0]:
                self.modele.initPartie(rep[2][1][0][1])
                self.vue.initPartie(self.modele)
                self.vue.canevasEspace.bind("<Button>",self.vue.changeCible)
                self.vue.root.after(10,self.timerJeu)
            elif rep[0]==0:
                self.vue.afficheListeJoueurs(rep[2])
                self.vue.root.after(10,self.timerAttend)
        else:
            print("Aucun serveur attache")
        
    def timerJeu(self):
        if self.serveur:
            self.cadre=self.cadre+1
            if self.actions:
                rep=self.serveur.faitAction([self.nom,self.cadre,self.actions])
                
            else:
                rep=self.serveur.faitAction([self.nom,self.cadre,0])
            self.actions=[]
            if rep[0]:
                for i in rep[2]:
                    if i in self.modele.actionsAFaire.keys():
                        for k in rep[2][i]:
                            for m in k:
                                self.modele.actionsAFaire[i].append(m)
                    else:
                        for k in rep[2][i]:
                            for m in k:
                                self.modele.actionsAFaire[i]=[m]
            if rep[1]=="attend":
                self.cadre=self.cadre-1
            else:
                self.modele.deplacerVaisseau()
                self.vue.afficheVaisseaux()
                self.modele.deplacerTravailleur()
                if self.cadre%30 == 0 :
                    self.modele.vaisseauAttaque()
                    self.modele.verifTourelle()
                if self.cadre%60 == 0 :
                   self.modele.calculRessource()
                   self.modele.processGov()
                if self.cadre%120 == 0 :
                   self.modele.calculUpkeep()
                self.modele.prochaineAction(self.cadre)
                if self.vue.planeteselection:
                    self.vue.affichePlanete()
                self.vue.afficheSelection()
                self.modele.processGov()
            self.vue.root.after(50,self.timerJeu)
        else:
            print("Aucun serveur connu")
        
if __name__ == '__main__':
    #s=input("1")
    print("DEBUT")
    c=Controleur()
    c.vue.root.mainloop()
    print("FIN")